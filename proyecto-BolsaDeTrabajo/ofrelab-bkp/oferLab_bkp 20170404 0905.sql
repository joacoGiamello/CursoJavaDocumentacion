-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.13-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema oferlab
--

CREATE DATABASE IF NOT EXISTS oferlab;
USE oferlab;

--
-- Definition of table `accesos`
--

DROP TABLE IF EXISTS `accesos`;
CREATE TABLE `accesos` (
  `ACC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ACC_DESCRIPCION` varchar(45) NOT NULL,
  `ROL_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ACC_CODIGO`),
  KEY `FK_accesos_rol` (`ROL_CODIGO`),
  CONSTRAINT `FK_accesos_rol` FOREIGN KEY (`ROL_CODIGO`) REFERENCES `roles` (`ROL_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accesos`
--

/*!40000 ALTER TABLE `accesos` DISABLE KEYS */;
/*!40000 ALTER TABLE `accesos` ENABLE KEYS */;


--
-- Definition of table `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE `alumnos` (
  `CIU_CODIGO` int(10) unsigned DEFAULT NULL,
  `ALU_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PRO_CODIGO` int(10) unsigned DEFAULT NULL,
  `PAIS_CODIGO` int(10) unsigned DEFAULT NULL,
  `ALU_CALLE` varchar(45) DEFAULT NULL,
  `ALU_NUMERO` varchar(45) DEFAULT NULL,
  `ALU_PISO` varchar(45) DEFAULT NULL,
  `ALU_TELEFONOPARTICULAR` varchar(45) DEFAULT NULL,
  `ALU_TELEFONOALTERNATIVO` varchar(45) DEFAULT NULL,
  `ALU_NOMBRE` varchar(45) DEFAULT NULL,
  `ALU_APELLIDO` varchar(45) DEFAULT NULL,
  `CAR_ID` int(10) unsigned DEFAULT NULL,
  `ALU_CUITDNI` int(10) unsigned DEFAULT NULL,
  `ALU_CUITDOSDIGITOS` int(10) unsigned DEFAULT NULL,
  `ALU_CUITDIGITOVERIFICADO` int(10) unsigned DEFAULT NULL,
  `CUR_ID` int(10) unsigned DEFAULT NULL,
  `ALU_ESTADOCARRERA` int(10) unsigned DEFAULT NULL,
  `ESTC_ID` int(10) unsigned DEFAULT NULL,
  `ALU_FECHANACIMIENTO` date DEFAULT NULL,
  `ALU_INSTITUTO` varchar(45) DEFAULT NULL,
  `ALU_NACIONALIDAD` varchar(45) DEFAULT NULL,
  `ALU_NUMERODOCUMENTO` int(10) unsigned DEFAULT NULL,
  `ALU_PROMEDIOCONAPLAZOS` float DEFAULT NULL,
  `ALU_SEXO` varchar(1) DEFAULT NULL,
  `ALU_STATUS` varchar(45) DEFAULT NULL,
  `TDOC_ID` int(10) unsigned DEFAULT NULL,
  `ALU_EMAIL` varchar(45) NOT NULL,
  `PART_CODIGO` int(10) unsigned NOT NULL,
  `ALU_LEGFINAL` int(10) unsigned NOT NULL,
  `ALU_LEGINICIAL` int(10) unsigned NOT NULL,
  `ALU_DTO` varchar(45) NOT NULL,
  PRIMARY KEY (`ALU_CODIGO`),
  KEY `FK_alumnos_ciudades` (`CIU_CODIGO`),
  CONSTRAINT `FK_alumnos_ciudades` FOREIGN KEY (`CIU_CODIGO`) REFERENCES `ciudades` (`CIU_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alumnos`
--

/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
INSERT INTO `alumnos` (`CIU_CODIGO`,`ALU_CODIGO`,`PRO_CODIGO`,`PAIS_CODIGO`,`ALU_CALLE`,`ALU_NUMERO`,`ALU_PISO`,`ALU_TELEFONOPARTICULAR`,`ALU_TELEFONOALTERNATIVO`,`ALU_NOMBRE`,`ALU_APELLIDO`,`CAR_ID`,`ALU_CUITDNI`,`ALU_CUITDOSDIGITOS`,`ALU_CUITDIGITOVERIFICADO`,`CUR_ID`,`ALU_ESTADOCARRERA`,`ESTC_ID`,`ALU_FECHANACIMIENTO`,`ALU_INSTITUTO`,`ALU_NACIONALIDAD`,`ALU_NUMERODOCUMENTO`,`ALU_PROMEDIOCONAPLAZOS`,`ALU_SEXO`,`ALU_STATUS`,`TDOC_ID`,`ALU_EMAIL`,`PART_CODIGO`,`ALU_LEGFINAL`,`ALU_LEGINICIAL`,`ALU_DTO`) VALUES 
 (1,8,0,0,'no tiene','0','no tiene','no tiene','no tiene','Jorge','Riquelme',0,0,0,0,0,0,0,'1978-06-07','no tiene','Argentina',33213432,0,'M','',1,'riquelme@jorgito.com.ar.net.no',0,0,0,'no tiene'),
 (1,10,0,0,'no tiene','0','no tiene','no tiene','no tiene','Fernando','Mano',0,0,0,0,0,0,0,'1956-08-04','no tiene','Argentina',23456789,0,'M','',1,'mano@hotmail.com',0,0,0,'no tiene'),
 (1,11,0,0,'no tiene','0','no tiene','no tiene','no tiene','Fede','Burgos',0,0,0,0,0,0,0,'1987-09-05','no tiene','Argentina',6575743,0,'M','',1,'gdhdhgdfgdsga',0,0,0,'no tiene'),
 (1,12,0,0,'no tiene','0','no tiene','no tiene','no tiene','gfdg','gdfg',0,0,0,0,0,0,0,'1984-07-12','no tiene','hgjf',4354645,0,'M','',2,'dhfgdjghjgh',0,0,0,'no tiene'),
 (1,13,0,0,'no tiene','0','no tiene','no tiene','no tiene','gfdgfd','fd4e35',0,0,0,0,0,0,0,'1983-07-14','no tiene','fhkgd',534735,0,'M','',2,'dsgfdhgfdhf',0,0,0,'no tiene'),
 (1,14,0,0,'no tiene','0','no tiene','no tiene','no tiene','roberta','Mano',0,0,0,0,0,0,0,'1983-08-12','no tiene','fsdfsfsf',4354332,0,'F','',1,'fgddgfdg',0,0,0,'no tiene');
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;


--
-- Definition of table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
CREATE TABLE `cargos` (
  `CAR_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CAR_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`CAR_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cargos`
--

/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;


--
-- Definition of table `carreras`
--

DROP TABLE IF EXISTS `carreras`;
CREATE TABLE `carreras` (
  `CAR_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CAR_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`CAR_CODIGO`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carreras`
--

/*!40000 ALTER TABLE `carreras` DISABLE KEYS */;
INSERT INTO `carreras` (`CAR_CODIGO`,`CAR_DESCRIPCION`) VALUES 
 (7,'ING ELECTRICA'),
 (10,'ING MECANICA'),
 (11,'ING ELECTRONICA');
/*!40000 ALTER TABLE `carreras` ENABLE KEYS */;


--
-- Definition of table `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
CREATE TABLE `ciudades` (
  `CIU_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CIU_DESCRIPCION` varchar(45) NOT NULL,
  `PAR_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CIU_ID`),
  KEY `FK_ciudades_partidos` (`PAR_ID`),
  CONSTRAINT `FK_ciudades_partidos` FOREIGN KEY (`PAR_ID`) REFERENCES `partidos` (`PAR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ciudades`
--

/*!40000 ALTER TABLE `ciudades` DISABLE KEYS */;
INSERT INTO `ciudades` (`CIU_ID`,`CIU_DESCRIPCION`,`PAR_ID`) VALUES 
 (1,'BAIRES',1);
/*!40000 ALTER TABLE `ciudades` ENABLE KEYS */;


--
-- Definition of table `conocimientosdeidiomas`
--

DROP TABLE IF EXISTS `conocimientosdeidiomas`;
CREATE TABLE `conocimientosdeidiomas` (
  `CONIDI_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CONIDI_NIVELDECONOCIMIENTO` int(10) unsigned NOT NULL,
  `IDIO_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CONIDI_CODIGO`),
  KEY `FK_conocimientosdeidiomas_idioma` (`IDIO_CODIGO`),
  CONSTRAINT `FK_conocimientosdeidiomas_idioma` FOREIGN KEY (`IDIO_CODIGO`) REFERENCES `idiomas` (`IDIO_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conocimientosdeidiomas`
--

/*!40000 ALTER TABLE `conocimientosdeidiomas` DISABLE KEYS */;
/*!40000 ALTER TABLE `conocimientosdeidiomas` ENABLE KEYS */;


--
-- Definition of table `conocimientosdesoftware`
--

DROP TABLE IF EXISTS `conocimientosdesoftware`;
CREATE TABLE `conocimientosdesoftware` (
  `CON_CODGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CON_NIVELDECONOCIMIENTO` int(10) unsigned NOT NULL,
  `SOFT_CODIGO` int(10) unsigned NOT NULL,
  `CUR_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CON_CODGO`),
  KEY `FK_CONOCIMIENTOSDESOFTWARE_CURRICULUMVITAE` (`CUR_CODIGO`),
  CONSTRAINT `FK_CONOCIMIENTOSDESOFTWARE_CURRICULUMVITAE` FOREIGN KEY (`CUR_CODIGO`) REFERENCES `curriculumsvitae` (`CUR_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conocimientosdesoftware`
--

/*!40000 ALTER TABLE `conocimientosdesoftware` DISABLE KEYS */;
/*!40000 ALTER TABLE `conocimientosdesoftware` ENABLE KEYS */;


--
-- Definition of table `contactos`
--

DROP TABLE IF EXISTS `contactos`;
CREATE TABLE `contactos` (
  `CONT_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CIU_CODIGO` int(10) unsigned DEFAULT NULL,
  `PRO_CODIGO` int(10) unsigned DEFAULT NULL,
  `PAIS_CODIGO` int(10) unsigned DEFAULT NULL,
  `CONT_CALLE` varchar(45) DEFAULT NULL,
  `CONT_NUMERO` varchar(45) DEFAULT NULL,
  `CONT_PISO` varchar(45) DEFAULT NULL,
  `CONT_TELEFONOPARTICULAR` varchar(45) DEFAULT NULL,
  `CONT_TELEFONOALTERNATIVO` varchar(45) DEFAULT NULL,
  `CONT_NOMBRE` varchar(45) DEFAULT NULL,
  `CONT_APELLIDO` varchar(45) DEFAULT NULL,
  `CAR_CODIGO` int(10) unsigned DEFAULT NULL,
  `EMP_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CONT_CODIGO`),
  KEY `FK_contactos_empresa` (`EMP_CODIGO`),
  CONSTRAINT `FK_contactos_empresa` FOREIGN KEY (`EMP_CODIGO`) REFERENCES `empresas` (`EMP_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactos`
--

/*!40000 ALTER TABLE `contactos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contactos` ENABLE KEYS */;


--
-- Definition of table `convenios`
--

DROP TABLE IF EXISTS `convenios`;
CREATE TABLE `convenios` (
  `CONV_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CONV_DURACION` int(10) unsigned NOT NULL,
  `CONV_FECHAINICIO` date NOT NULL,
  `FIR_CODIGO` int(10) unsigned NOT NULL,
  `CONV_NUMERODEACUERDO` int(10) unsigned NOT NULL,
  `CONV_PORCENTAJE` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CONV_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `convenios`
--

/*!40000 ALTER TABLE `convenios` DISABLE KEYS */;
/*!40000 ALTER TABLE `convenios` ENABLE KEYS */;


--
-- Definition of table `curriculumsvitae`
--

DROP TABLE IF EXISTS `curriculumsvitae`;
CREATE TABLE `curriculumsvitae` (
  `CUR_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CUR_ANIOFINSECUNDARIO` int(10) unsigned NOT NULL,
  `CUR_CONFIDENCIALIDAD` char(1) NOT NULL,
  `CUR_INSTITUTO` varchar(45) NOT NULL,
  `TIPS_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CUR_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `curriculumsvitae`
--

/*!40000 ALTER TABLE `curriculumsvitae` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculumsvitae` ENABLE KEYS */;


--
-- Definition of table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `ssn` varchar(45) NOT NULL,
  `cust_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `address` varchar(45) NOT NULL,
  PRIMARY KEY (`ssn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`ssn`,`cust_name`,`address`) VALUES 
 ('111-11-1111',0x44616E69656C61,'Paris 322 Haedo'),
 ('111-11-1112',0x4F74726F204E6F6D627265,'Otra'),
 ('111-11-1122',0x6A6F656C,'nose-nose'),
 ('111-11-1123',0x436F736D6546756C616E69746F,'Springfield'),
 ('22333444',0x4761627269656C69746F,'Solari -Moron'),
 ('32',0x646464,'dsfdsfsdf'),
 ('666',0x66756C616E69746F73,'quety'),
 ('6768',0x66,'fghfh'),
 ('JUNK',0x4761627269656C69746F,'Moron');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


--
-- Definition of table `diashoras`
--

DROP TABLE IF EXISTS `diashoras`;
CREATE TABLE `diashoras` (
  `DH_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DH_HORAINICIO` int(10) unsigned NOT NULL,
  `DH_HORAFIN` int(10) unsigned NOT NULL,
  `DH_MININICIO` int(10) unsigned NOT NULL,
  `DH_MINFIN` int(10) unsigned NOT NULL,
  `JOR_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`DH_CODIGO`),
  KEY `FK_diashoras_joranada` (`JOR_CODIGO`),
  CONSTRAINT `FK_diashoras_joranada` FOREIGN KEY (`JOR_CODIGO`) REFERENCES `jornadaslaborales` (`JOR_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diashoras`
--

/*!40000 ALTER TABLE `diashoras` DISABLE KEYS */;
/*!40000 ALTER TABLE `diashoras` ENABLE KEYS */;


--
-- Definition of table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
CREATE TABLE `empresas` (
  `EMP_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CIU_CODIGO` int(10) unsigned DEFAULT NULL,
  `PRO_CODIGO` int(10) unsigned DEFAULT NULL,
  `PAIS_CODIGO` int(10) unsigned DEFAULT NULL,
  `EMP_CALLE` varchar(45) DEFAULT NULL,
  `EMP_NUMERO` varchar(45) DEFAULT NULL,
  `EMP_PISO` varchar(45) DEFAULT NULL,
  `EMP_TELEFONOPARTICULAR` varchar(45) DEFAULT NULL,
  `EMP_TELEFONOALTERNATIVO` varchar(45) DEFAULT NULL,
  `EMP_NOMBRE` varchar(45) DEFAULT NULL,
  `CONV_ALU_ID` int(10) unsigned DEFAULT NULL,
  `CONV_FAC_ID` int(10) unsigned DEFAULT NULL,
  `EMP_CUIT_CODVERIFICADOR` int(10) unsigned DEFAULT NULL,
  `EMP_CUIT_DOSDIGITOS` int(10) unsigned DEFAULT NULL,
  `EMP_CUIT_EMPRESA` int(10) unsigned DEFAULT NULL,
  `EMP_DOMICILIOLEGAL` varchar(45) DEFAULT NULL,
  `EMP_FAX` varchar(45) DEFAULT NULL,
  `EMP_FECHADEACTUALIZACION` date DEFAULT NULL,
  `RUB_ID` int(10) unsigned DEFAULT NULL,
  `EMP_ZONADETRABAJO` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`EMP_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `empresas`
--

/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;


--
-- Definition of table `estadosciviles`
--

DROP TABLE IF EXISTS `estadosciviles`;
CREATE TABLE `estadosciviles` (
  `ESTC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ESTC_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`ESTC_CODIGO`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estadosciviles`
--

/*!40000 ALTER TABLE `estadosciviles` DISABLE KEYS */;
INSERT INTO `estadosciviles` (`ESTC_CODIGO`,`ESTC_DESCRIPCION`) VALUES 
 (1,'EstadoCivil1_test'),
 (2,'EstadoCivil2_test'),
 (3,'EstadoCivil3_test'),
 (4,'EstadoCivil4_test');
/*!40000 ALTER TABLE `estadosciviles` ENABLE KEYS */;


--
-- Definition of table `experienciaslaborales`
--

DROP TABLE IF EXISTS `experienciaslaborales`;
CREATE TABLE `experienciaslaborales` (
  `EXP_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EXP_ANTIGUEDAD` int(10) unsigned NOT NULL,
  `EXP_PUESTO` varchar(45) NOT NULL,
  `TIPC_CODIGO` int(10) unsigned NOT NULL,
  `CUR_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`EXP_CODIGO`),
  KEY `FK_experienciaslaborales_tipoDecontrato` (`TIPC_CODIGO`),
  KEY `FK_experienciaslaborales_curriculumVitae` (`CUR_CODIGO`),
  CONSTRAINT `FK_experienciaslaborales_curriculumVitae` FOREIGN KEY (`CUR_CODIGO`) REFERENCES `curriculumsvitae` (`CUR_CODIGO`),
  CONSTRAINT `FK_experienciaslaborales_tipoDecontrato` FOREIGN KEY (`TIPC_CODIGO`) REFERENCES `tiposdecontratos` (`TIPC_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `experienciaslaborales`
--

/*!40000 ALTER TABLE `experienciaslaborales` DISABLE KEYS */;
/*!40000 ALTER TABLE `experienciaslaborales` ENABLE KEYS */;


--
-- Definition of table `idiomas`
--

DROP TABLE IF EXISTS `idiomas`;
CREATE TABLE `idiomas` (
  `IDIO_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDIO_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`IDIO_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `idiomas`
--

/*!40000 ALTER TABLE `idiomas` DISABLE KEYS */;
/*!40000 ALTER TABLE `idiomas` ENABLE KEYS */;


--
-- Definition of table `jornadaslaborales`
--

DROP TABLE IF EXISTS `jornadaslaborales`;
CREATE TABLE `jornadaslaborales` (
  `JOR_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `JOR_DIAHORA` int(10) unsigned NOT NULL,
  `JOR_FECHADESDE` date NOT NULL,
  PRIMARY KEY (`JOR_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jornadaslaborales`
--

/*!40000 ALTER TABLE `jornadaslaborales` DISABLE KEYS */;
/*!40000 ALTER TABLE `jornadaslaborales` ENABLE KEYS */;


--
-- Definition of table `ofertalaboral`
--

DROP TABLE IF EXISTS `ofertalaboral`;
CREATE TABLE `ofertalaboral` (
  `OFER_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CAR_CODIGO` int(10) unsigned NOT NULL,
  `CONT_CODIGO` int(10) unsigned NOT NULL,
  `EMP_CODIGO` int(10) unsigned NOT NULL,
  `OFER_ESTADO` int(10) unsigned NOT NULL,
  `JOR_CODIGO` int(10) unsigned NOT NULL,
  `OFER_REFERENCIA` int(10) unsigned NOT NULL,
  PRIMARY KEY (`OFER_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ofertalaboral`
--

/*!40000 ALTER TABLE `ofertalaboral` DISABLE KEYS */;
/*!40000 ALTER TABLE `ofertalaboral` ENABLE KEYS */;


--
-- Definition of table `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE `paises` (
  `PAIS_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PAIS_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`PAIS_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paises`
--

/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;


--
-- Definition of table `partidos`
--

DROP TABLE IF EXISTS `partidos`;
CREATE TABLE `partidos` (
  `PAR_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PAR_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`PAR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partidos`
--

/*!40000 ALTER TABLE `partidos` DISABLE KEYS */;
INSERT INTO `partidos` (`PAR_ID`,`PAR_DESCRIPCION`) VALUES 
 (1,'MORON'),
 (2,'EZEIZA'),
 (3,'TRES DE FEBRERO'),
 (4,'Partido1_test'),
 (5,'Partido2_test'),
 (6,'Partido3_test'),
 (7,'Partido4_test'),
 (8,'Partido1_test'),
 (9,'Partido2_test'),
 (10,'Partido3_test'),
 (11,'Partido4_test'),
 (12,'Partido1_test'),
 (13,'Partido2_test'),
 (14,'Partido3_test'),
 (15,'Partido4_test'),
 (16,'Partido1_test'),
 (17,'Partido2_test'),
 (18,'Partido3_test'),
 (19,'Partido4_test'),
 (20,'Partido1_test'),
 (21,'Partido2_test'),
 (22,'Partido3_test'),
 (23,'Partido4_test'),
 (24,'Partido1_test'),
 (25,'Partido2_test'),
 (26,'Partido3_test'),
 (27,'Partido4_test'),
 (28,'Partido1_test'),
 (29,'Partido2_test'),
 (30,'Partido3_test'),
 (31,'Partido4_test'),
 (32,'Partido1_test'),
 (33,'Partido2_test'),
 (34,'Partido3_test'),
 (35,'Partido4_test'),
 (36,'Partido1_test'),
 (37,'Partido2_test'),
 (38,'Partido3_test'),
 (39,'Partido4_test'),
 (40,'Partido1_test'),
 (41,'Partido2_test'),
 (42,'Partido3_test'),
 (43,'Partido4_test');
/*!40000 ALTER TABLE `partidos` ENABLE KEYS */;


--
-- Definition of table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
CREATE TABLE `provincias` (
  `PROV_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PROV_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`PROV_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provincias`
--

/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;


--
-- Definition of table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `ROL_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ROL_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`ROL_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


--
-- Definition of table `rubros`
--

DROP TABLE IF EXISTS `rubros`;
CREATE TABLE `rubros` (
  `RUB_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RUB_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`RUB_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rubros`
--

/*!40000 ALTER TABLE `rubros` DISABLE KEYS */;
/*!40000 ALTER TABLE `rubros` ENABLE KEYS */;


--
-- Definition of table `softwares`
--

DROP TABLE IF EXISTS `softwares`;
CREATE TABLE `softwares` (
  `SOFT_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SOFT_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`SOFT_CODIGO`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `softwares`
--

/*!40000 ALTER TABLE `softwares` DISABLE KEYS */;
INSERT INTO `softwares` (`SOFT_CODIGO`,`SOFT_DESCRIPCION`) VALUES 
 (1,'Adobe Illustrator'),
 (2,'Adobe Photoshop'),
 (4,'Excel'),
 (5,'PowerPoint'),
 (6,'Autocad'),
 (8,'Word');
/*!40000 ALTER TABLE `softwares` ENABLE KEYS */;


--
-- Definition of table `tiposdecontratos`
--

DROP TABLE IF EXISTS `tiposdecontratos`;
CREATE TABLE `tiposdecontratos` (
  `TIPC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPC_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`TIPC_CODIGO`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tiposdecontratos`
--

/*!40000 ALTER TABLE `tiposdecontratos` DISABLE KEYS */;
INSERT INTO `tiposdecontratos` (`TIPC_CODIGO`,`TIPC_DESCRIPCION`) VALUES 
 (1,'LEY MARCO'),
 (2,'velez'),
 (3,'Ley marco'),
 (4,'primaria'),
 (5,'PRIMARIA'),
 (6,'PLANTA');
/*!40000 ALTER TABLE `tiposdecontratos` ENABLE KEYS */;


--
-- Definition of table `tiposdedocumentos`
--

DROP TABLE IF EXISTS `tiposdedocumentos`;
CREATE TABLE `tiposdedocumentos` (
  `TIPDOC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPDOC_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`TIPDOC_CODIGO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tiposdedocumentos`
--

/*!40000 ALTER TABLE `tiposdedocumentos` DISABLE KEYS */;
INSERT INTO `tiposdedocumentos` (`TIPDOC_CODIGO`,`TIPDOC_DESCRIPCION`) VALUES 
 (1,'DNI'),
 (2,'LE'),
 (3,'LC');
/*!40000 ALTER TABLE `tiposdedocumentos` ENABLE KEYS */;


--
-- Definition of table `tiposdesecundarios`
--

DROP TABLE IF EXISTS `tiposdesecundarios`;
CREATE TABLE `tiposdesecundarios` (
  `TIPSEC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPSEC_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`TIPSEC_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tiposdesecundarios`
--

/*!40000 ALTER TABLE `tiposdesecundarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiposdesecundarios` ENABLE KEYS */;


--
-- Definition of table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `USU_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CIU_CODIGO` int(10) unsigned DEFAULT NULL,
  `PRO_CODIGO` int(10) unsigned DEFAULT NULL,
  `PAIS_CODIGO` int(10) unsigned DEFAULT NULL,
  `USU_CALLE` varchar(45) DEFAULT NULL,
  `USU_NUMERO` varchar(45) DEFAULT NULL,
  `USU_PISO` varchar(45) DEFAULT NULL,
  `USU_TELEFONOPARTICULAR` varchar(45) DEFAULT NULL,
  `USU_TELEFONOALTERNATIVO` varchar(45) DEFAULT NULL,
  `USU_NOMBRE` varchar(45) DEFAULT NULL,
  `USU_APELLIDO` varchar(45) DEFAULT NULL,
  `ROL_CODIGO` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`USU_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

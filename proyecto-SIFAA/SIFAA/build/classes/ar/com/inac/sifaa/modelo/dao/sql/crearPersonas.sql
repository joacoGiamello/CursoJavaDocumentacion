DROP TABLE IF EXISTS `sifaa`.`personas`;
CREATE TABLE  `sifaa`.`personas` (
  `PER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PART_ID` int(10) unsigned DEFAULT NULL,
  `PER_NOMBRE` varchar(45) NOT NULL,
  `PER_APELLIDO` varchar(45) NOT NULL,
  `PER_DIRECCION` varchar(45) DEFAULT NULL,
  `PER_TELEFONOPART` varchar(45) DEFAULT NULL,
  `PER_TELEFONOCEL` varchar(45) DEFAULT NULL,
  `PER_DNI` varchar(45) DEFAULT NULL,
  `PER_CUIL` varchar(45) DEFAULT NULL,
  `PER_CODIGODEBARRAS` varchar(45) DEFAULT NULL,
  `PER_FECHADENACIMIENTO` date DEFAULT NULL,
  `PER_EMAIL` varchar(45) DEFAULT NULL,
  `PER_IOSFA` varchar(45) DEFAULT NULL,
  `PER_LUGARDETRABAJO` varchar(45) DEFAULT NULL,
  `PER_PATHFOTO` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PER_ID`),
  KEY `FK_personas_partido` (`PART_ID`),
  CONSTRAINT `FK_personas_partido` FOREIGN KEY (`PART_ID`) REFERENCES `partidos` (`PART_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
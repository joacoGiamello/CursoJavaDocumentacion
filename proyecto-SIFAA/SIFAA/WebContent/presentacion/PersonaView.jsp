<%@page import="ar.com.inac.sifaa.modelo.Partido"%>
<%@page import="java.util.List"%>
<% //TODO Agustin acomodame estas tablas para que se vean lindas %>
  		<div class="input-group col-xs-8">  		
	    	<span style="width: 150px;" class="input-group-addon">Nro de DIPFA</span>
	    	<input id="idDISPFA"  type="text" class="form-control" name="txtDIPFA" placeholder="Ingrese Nro de DIPFA"> 
	    </div>

  		<div class="input-group col-xs-8">  		
	    	<span style="width: 150px;" class="input-group-addon">Nombres</span>
	    	<input id="idNombre"  type="text" class="form-control" name="txtNombre" placeholder="Ingrese nombres"> 
	    </div>
	    
	    <div class="input-group col-xs-8">	
	    	<span style="width: 150px;" class="input-group-addon">Apellido</span>
	    	<input id="idApellido" type="text" class="form-control col-sm" name="txtApellido" placeholder="Ingrese apellido"></br>
	    </div>
	    <div class="input-group col-xs-8">	
	    	<span style="width: 150px;" class="input-group-addon">DNI</span>
	    	<input id="idDNI" type="text" class="form-control col-sm" name="txtDNI" placeholder="Ingrese DNI"></br>
	    </div>
	    <div class="input-group col-xs-8">	
	    	<span style="width: 150px;" class="input-group-addon">CUIL</span>
	    	<input id="idCUIL" type="text" class="form-control col-sm" name="txtCUIL" placeholder="Ingrese CUIL"></br>
	    </div>
	    
	    <div class="input-group col-xs-8">	
    		<span style="width: 150px;" class="input-group-addon">Residencia Domicilio</span>
	    	<input id="idDireccion" type="text" class="form-control" name="txtDireccion" placeholder="Ingrese Residencia Domicilio"></br>
	    </div>

    <div class="form-group col-xs-8">
	    <label style="width: 150px;" for="cmbPartido">Seleccione partido</label>
	  	<select class="form-control " id="idPartido" name ="cmbPartido">
		  	<%List<Partido> partidos = (List<Partido>) request.getAttribute("partidos"); 
		  	 for(Partido partido:partidos){%>
		  	 	<option><%=partido.getCodigo()+"-" + partido.getDescripcion().trim() %></option>
		  	 <%} %>
  	 
	  	</select>
	 </div>

	    <div class="input-group col-xs-8">	
    		<span style="width: 150px;" class="input-group-addon">Tel�fono</span>
	    	<input id="idTelefono" type="text" class="form-control" name="txtTelefono" placeholder="Ingrese Telefono"></br>
	    </div>
	    <div class="input-group col-xs-8">	
    		<span style="width: 150px;" class="input-group-addon">Tel�fono M�vil</span>
	    	<input id="idTelefonoCel" type="text" class="form-control" name="txtTelefonoCel" placeholder="Ingrese Telefono Celular"></br>
	    </div>
	    
	    <div class="input-group col-xs-8">	
		    <span style="width: 150px;" class="input-group-addon">Email</i></span>
		    <input id="email" type="text" class="form-control" name="txtEmail" placeholder="Ingrese Email"></br>
	    </div>
	    
	    	    <div class="input-group col-xs-8">	
		    <span style="width: 150px;" class="input-group-addon">Domicilio</i></span>
		    <input id="idDomicilio" type="text" class="form-control" name="txtDomicilio" placeholder="Ingrese Domicilio"></br>
	    </div>
  		
	    <div class="input-group col-xs-8">	
		    <span style="width: 150px;" class="input-group-addon">Lugar de trabajo</i></span>
		    <input id="idLugarDeTrabajo" type="text" class="form-control" name="txtLugarDeTrabajo" placeholder="ingrese Lugar de trabajo"></br>
	    </div>

	    <div class="input-group col-xs-8">	
		    <span style="width: 150px;" class="input-group-addon">Foto</i></span>
		    <input id="idFoto" type="text" class="form-control" name="txtFoto" placeholder="Ingrese path de la foto"></br>
	    </div>
	     <div class="file">
                     <label class="file-label">
                       <input class="file-input" type="file" name="resume">
                        <span class="file-cta">
                           <span class="file-icon">
                              <i class="fas fa-upload"></i>
                           </span>
                        <span class="file-label">
                                <figure class="image is-128x128">
                                
                               <img src="https://bulma.io/images/placeholders/128x128.png">
                          
                              </figure>
                      </span>
                    </span>
                  </label>
               </div>
	    

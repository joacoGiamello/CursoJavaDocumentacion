<%@page import="ar.com.inac.sifaa.modelo.Persona"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:include page="Encabezado.jsp"/>
<title>accion</title>
</head>
<body style='margin:30px'>
		
<%  String strAccion=(String)request.getAttribute("accion");
	Persona per = (Persona)request.getAttribute("persona");
	%>
	
	<% 
		if(strAccion.equals("M")){ %>
		<form class="form-horizontal" action="AccionPersonaController" method="post">
		<input type="hidden" name="txtCodigo"  value="<%=per.getCodigo()%>"/>
		<input type="hidden" name="txtAccion"  value="<%=strAccion%>"/>
		<h1>MODIFICAR</h1>
		</br>
		</br>C�digo: <%=per.getCodigo() %> </br>
		
		<div class="input-group col-xs-8">  		
	    	<span style="width: 150px;" class="input-group-addon">Nro de DIPFA</span>
	    	<input id="idDISPFA"  type="text" class="form-control" name="txtDIPFA" value="<%=per.getIosfa()%>"> 
	    </div>
	    
		</br>Nro de DIFFA <input type="text" name="txtDIPFA" class="form-control" value="<%=per.getIosfa()%>"/>
		</br>Nombres <input type="text" name="txtNombre"    value="<%=per.getNombre()%>"/>
		</br>Apellido <input type="text" name="txtApellido"  value="<%=per.getApellido()%>"/>
		</br>Direcci�n <input type="text" name="txtDireccion" value="<%=per.getDireccion()%>"/>
		</br>DNI <input type="text" name="txtDNI" value="<%=per.getDni()%>"/>
		</br>CUIL <input type="text" name="txtCUIL" value="<%=per.getCuil()%>"/>
		</br>Tel�fono <input type="text" name="txtTelefono" value="<%=per.getTelefonoParticular()%>"/>
		</br>Tel�fono M�vil <input type="text" name="txtTelefonoMovil" value="<%=per.getTelefonoCelular()%>"/>
		</br>E-mail <input type="text" name="txtEmail" value="<%=per.getEmail()%>"/>
		</br>Lugar de Trabajo <input type="text" name="txtLugarDeTrabajo" value="<%=per.getLugarDeTrabajo()%>"/>
		<h1>Veh�culo</h1>

		</br>
		<input value="Aceptar"  type="submit" name="btnAceptar"/>
		<input value="Cancelar" type="submit" name="btnCancelar"/> 
		</form>
		
				
	<%} else if(strAccion.equals("V")){ %>
		<form action="AccionPersonaController" method="post">
		<input type="hidden" name="txtCodigo"  value="<%=per.getCodigo()%>"/>
		<input type="hidden" name="txtAccion"  value="<%=strAccion%>"/>
		<h1>VER</h1>
		</br>
		</br>
		
		C�digo  	<%=per.getCodigo() 	 %> </br>
		Nro de DIPFA<%=per.getIosfa()	 %> </br>
		Nombres		<%=per.getNombre() 	 %> </br>
		Apellido	<%=per.getApellido() %> </br>
		DNI			<%=per.getDni()		 %> </br>
		CUIL 		<%=per.getCuil()	%> </br>
		Direcci�n	<%=per.getDireccion()%> </br>
		Partido 	<%=per.getPartido().getDescripcion()%> </br>
		Tel�fono	<%=per.getTelefonoParticular()%> </br>
		Tel�fono M�vil	<%=per.getTelefonoCelular()%> </br>
		E-mail		<%=per.getEmail()	%> </br>
		Lugar de Trabajo	<%=per.getLugarDeTrabajo()%> </br>
		
				
		</br>
		<input value="Aceptar"  type="submit" name="btnAceptar">
		<input value="Cancelar" type="submit" name="btnCancelar">
		</form> 
	<%} else if(strAccion.equals("E")){ %>
		<form action="AccionPersonaController" method="post">
		<input type="hidden" name="txtCodigo"  value="<%=per.getCodigo()%>"/>
		<input type="hidden" name="txtAccion"  value="<%=strAccion%>"/>
		<h1>ELIMINAR</h1>
		</br>
		</br>
		<h1>Desea eliminar el registro ...</h1>
		</br>
		</br>
		C�digo  	<%=per.getCodigo() %> </br>
		Nro de DIPFA<%=per.getIosfa()	 %> </br>
		Nombre 		<%=per.getNombre() 	 %> </br>
		Apellido	<%=per.getApellido() %> </br>
		Direcci�n	<%=per.getDireccion()%> </br>
		</br>
		<input value="Aceptar"  type="submit" name="btnAceptar">
		<input value="Cancelar" type="submit" name="btnCancelar">
		</form> 	
	<%} else if(strAccion.equals("F")){ %>
		<form action="SubirFotoController" enctype='multipart/form-data'  method="post">
		<input type="hidden" name="txtCodigo"  value="<%=per.getCodigo()%>"/>
		<input type="hidden" name="txtAccion"  value="<%=strAccion%>"/>
		<h1>Agregar Foto</h1>
		</br>
		</br>
		<h1>Desea agregar foto ...</h1>
				</br>
		</br>
		C�digo  	<%=per.getCodigo() %> </br>
		Nro de DIPFA<%=per.getIosfa()	 %> </br>
		Nombre 		<%=per.getNombre() 	 %> </br>
		Apellido	<%=per.getApellido() %> </br>
		Direcci�n	<%=per.getDireccion()%> </br>
		<input type="file" name="file" /><br/>
		<input type="submit" value="Upload" />
		</form>
	<%}%>

</body>
</html>
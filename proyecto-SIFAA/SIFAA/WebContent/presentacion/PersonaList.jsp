<%@page import="ar.com.inac.sifaa.modelo.Persona"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="Encabezado.jsp"/>
<body style='margin:30px'>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

	<table border= 1px>
		<tr>
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Foto</th>
			<th>Modificar</th>
			<th>Ver</th>
			<th>Eliminar</th>
		</tr>
		<%
		List<Persona> personas = (List)request.getAttribute("personasList");
		for (Persona persona : personas) {			
		%>
		<tr>
			<th>
				<%=persona.getCodigo() %>
			</th>
			<td>
				<%=persona.getNombre() %>
			</td>
			<td>
				<%=persona.getApellido() %>
			</td>
			
			<td>
				<%String url = "PersonaController?accion=F&codPersona=" + persona.getCodigo();
				  String strFoto= null;
				  if(persona.getPathFoto()==null)
					strFoto = "iconos/User.png";
				  else
					strFoto="fotosPersonas/" + persona.getPathFoto();
				  System.out.println("strFoto=" + strFoto);
				 %>
				<a href=<%=url%>><img title="SubirFoto" src="<%=strFoto%>" width="10%" heigh="10%"></a>
			</td>
			
			<td>
				<%url = "PersonaController?accion=M&codPersona=" + persona.getCodigo();%>
				<a href=<%=url%>><img title="Modificar" src="iconos/modificar.ico" ></a>				
			</td>
				 
			<td>
				<%url = "PersonaController?accion=V&codPersona=" + persona.getCodigo();%>
				<a href=<%=url%>><img title="ver" src="iconos/ver.ico"></a>				
			</td>
			<td>
				<%url = "PersonaController?accion=E&codPersona=" + persona.getCodigo();%>
				<a href=<%=url%>><img title="Eliminar" src="iconos/eliminar.ico"></a>
			</td>
			
		</tr>
		
		<%} %>
	</table>
	<%//TODO alguien - TERMINADO agregar el boton de home y atras agregar los graficos en src%>
	<table>
			<tr>
				<!-- va a ir al home -->
				<td>
					<a href="bootstrap/index.html"><img title="Volver al Home" src="iconos/Home.ico" ></a>	
				</td>
				<!-- tengo que ir al buscar -->
				<td>
					<a href="HomeController?codigo=1"><img title="atras" src="iconos/atras.png" ></a>
				</td>
			</tr>			
	</table>
</body>
</html>
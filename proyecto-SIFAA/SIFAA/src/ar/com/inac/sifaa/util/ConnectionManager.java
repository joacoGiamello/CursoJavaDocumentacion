package ar.com.inac.sifaa.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class ConnectionManager {
	private Session session;
	private Transaction transaction;
	private ResourceBundle resources;
	
	
	public boolean conectar()throws Exception{
		if (session == null|| !session.isOpen() )
			try {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		else return false;
	}
	
	public boolean conectar(String svr)throws Exception{
		if (session == null|| !session.isOpen() )
			try {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		else return false;
	}
	public Session getSession(){
		return session;
	}
	
	public boolean desconectar ()throws Exception{
		if ((session!=null)&&(session.isOpen())) {
			try {
				session.close();
				session = null;
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}else return false;
	}
	
	public boolean beginTx()throws Exception{
		if (session!=null){
				try {
					transaction = session.beginTransaction();
					return true;
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
		}else return false;
	}
	
	public void commitTx(){
		if (transaction!=null){
			transaction.commit();
			transaction = null;
		}
	}
	
	public void rollbackTx(){
		if (transaction!=null){
			transaction.rollback();
			transaction=null;
		}
	}
	
	
	}

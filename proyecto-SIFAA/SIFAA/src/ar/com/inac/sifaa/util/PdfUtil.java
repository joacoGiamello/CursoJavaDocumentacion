package ar.com.inac.sifaa.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;



public class PdfUtil {
	public static void crearPdf(String archivo, String texto) throws FileNotFoundException, DocumentException{
        // step 1
		Document document = new Document(PageSize.LETTER);
        // step 2
        
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(archivo));
        // step 3
        document.open();
        // step 4
        document.add(new Paragraph(texto));
        // step 5
        document.close();

	}
	
	public static Document crearPdfImagenYtexto(String pathArchivo, String pathImagen, String strTexto) throws DocumentException, MalformedURLException, IOException{
    	// step 1 ,
		// Parametros
		// 1- el tamanio de la pagina
		// 2- margen izquierdo
		// 3- margen derecho
		// 4- margen superio
		// 5- margen inferior
        Document document
            = new Document(PageSize.A4, 36f, 72f, 108f, 180f);

        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pathArchivo));
        writer.setCompressionLevel(0);
        // step 3
        document.open();
        // step 4
        // la posicion esta dada por x e y tomando como referenciaa el extremo inferior izquierdo
        Image img = Image.getInstance(pathImagen);
       
        img.setAbsolutePosition(50,750);
        writer.getDirectContent().addImage(img);
        
//        document.setHeader(new HeaderFooter(new Phrase("Anio de la tarasca "),null));
        
        Paragraph p = new Paragraph(strTexto);

        p.setAlignment(Element.ALIGN_JUSTIFIED);
        document.add(p);
        // step 5
        document.close();
		
		return document;
	}

}

package ar.com.inac.sifaa.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Contiene utiles para reflection de clases.
 * 
 * @author Santos Sanchez
 */
public class ReflectionUtils {
	
	/**
	 * Obtiene los atributos de una clase.
	 * 
	 * @param clase Clase de donde se obtendran los atributos
	 * @return Lista de {@link Field} con los atributos de la clase
	 */
	public List<Field> getAtributos(Class<?> clase) {
		 Field[] atributos = clase.getDeclaredFields();
		 
		 return new ArrayList<Field>(Arrays.asList(atributos));
	}
	
	
	/**
	 * Modifica los datos de los atributos del objeto 
	 * <code>objectoAModificar</code> con los datos de
	 * los atributos de <code>objectoModificado</code>.<br>
	 * Se modificaran solo los atributos que no est�n en null
	 * dentro del objeto <code>objectoModificado</code>.<br>
	 * Los objetos participantes deben est�r bien formados con
	 * sus correspondientes getters y setters.
	 * 
	 * @param objectoAModificar Objeto a modificar los atributos
	 * @param objectoModificado Objeto referencia para saber que atributos se modifican
	 * @return Objeto con los atributos modificados
	 * @throws Exception 
	 */
	public Object modificarDatosAtributos(Object objectoAModificar, Object objectoModificado) throws Exception {
		if (objectoAModificar.getClass() != objectoModificado.getClass()) {
			throw new Exception("Los objetos deben ser de la misma clase");
		}
		
		List<Field> listaAtributos = getAtributos(objectoModificado.getClass());
		for (Field atributo : listaAtributos) {
			Method getterAtributo = new PropertyDescriptor(atributo.getName(), objectoModificado.getClass()).getReadMethod();
			Method setterAtributo = new PropertyDescriptor(atributo.getName(), objectoModificado.getClass()).getWriteMethod();
			Object atributoAModificar = getterAtributo.invoke(objectoAModificar);
			Object atributoModificado = getterAtributo.invoke(objectoModificado);
			if(atributoModificado != null && !atributoModificado.equals(atributoAModificar)) {
				setterAtributo.invoke(objectoModificado, atributo.getType());
			}
		}
		
		return objectoAModificar;
	}
}

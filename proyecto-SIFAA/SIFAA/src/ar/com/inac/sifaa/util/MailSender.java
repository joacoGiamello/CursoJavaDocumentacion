package ar.com.inac.sifaa.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Envia mail utilizando solo cuentas de gmail
 * 
 *
 */
public class MailSender {

	/**
	 * Envia un mail
	 * @param accountFrom cuenta de gmail utilizada para enviar el mail
	 * @param password password de la cuenta utilizada
	 * @param subject titulo del correo
	 * @param content contenido del correo
	 * @param accountsTo cuentas a las cuales se envia el mail
	 */
	public static void send(String accountFrom, String password, String subject, String content, String... accountsTo){
		
	    try {
	        Properties props = new Properties();

	        props.put("mail.transport.protocol", "smtps");
	        props.put("mail.smtps.host", "smtp.gmail.com");
	        props.put("mail.smtps.auth", "true");

	        Session mailSession = Session.getDefaultInstance(props);
	        mailSession.setDebug(true);
	        Transport transport = mailSession.getTransport();

	        MimeMessage message = new MimeMessage(mailSession);

	
	        // message subject
	        message.setSubject(subject);
	        // message body
	        message.setContent(content, "text/plain");

	        for (String accountTo : accountsTo) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(accountTo));
			}

	        transport.connect("smtp.gmail.com", 465, accountFrom, password);

	        transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
	        transport.close();

	      } catch (AddressException e) {
	        e.printStackTrace();
	      }catch (NoSuchProviderException e) {
	  		e.printStackTrace();
	  	} catch (MessagingException e) {
	  		e.printStackTrace();
	  	}
	}
}

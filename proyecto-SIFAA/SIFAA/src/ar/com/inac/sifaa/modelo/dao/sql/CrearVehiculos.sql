DROP TABLE IF EXISTS `sifaa`.`vehiculos`;
CREATE TABLE  `sifaa`.`vehiculos` (
  `VEH_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PER_ID` int(10) unsigned NOT NULL,
  `VEH_MARCA` varchar(45) NOT NULL,
  `VEH_MODELO` varchar(45) NOT NULL,
  `VEH_COLOR` varchar(45) NOT NULL,
  `VEH_DOMINIO` varchar(45) NOT NULL,
  PRIMARY KEY (`VEH_ID`),
  KEY `FK_vehiculos_persona` (`PER_ID`),
  CONSTRAINT `FK_vehiculos_persona` FOREIGN KEY (`PER_ID`) REFERENCES `personas` (`PER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
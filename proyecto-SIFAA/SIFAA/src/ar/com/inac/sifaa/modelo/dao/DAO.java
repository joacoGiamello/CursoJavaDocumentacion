package ar.com.inac.sifaa.modelo.dao;



import java.util.List;

import org.hibernate.Session;



public interface DAO {
		public Object agregarModificar(Object obj, Session session)throws Exception;
		public Object eliminar(Object obj, Session session)throws Exception;
		public List leer(Object obj, Session session)throws Exception;
}

package ar.com.inac.sifaa.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ar.com.inac.sifaa.modelo.Persona;
import ar.com.inac.sifaa.modelo.dao.PersonaDao;
import ar.com.inac.sifaa.util.ConnectionManager;

/**
 * Servlet implementation class AccionController
 */
public class AccionPersonaController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccionPersonaController() {
        super();
        }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String strAccion = (String)request.getParameter("txtAccion");
		String strCodigo = (String)request.getParameter("txtCodigo");
		
		String strNombre = (String)request.getParameter("txtNombre");
		String strApellido = (String)request.getParameter("txtApellido");
		String strDireccion = (String)request.getParameter("txtDireccion");
		String strTelefono = (String)request.getParameter("txtTelefono");
		//se obtienen el resto de los campos
		String strDni		=(String)request.getParameter("txtDNI");
		String strDIPFA		=(String)request.getParameter("txtDIPFA");
		String strCUIL		=(String)request.getParameter("txtCUIL");
		
		
		String strAceptar = (String)request.getParameter("btnAceptar");
		String strCancelar = (String)request.getParameter("btnCancelar");
				
		System.out.println("Accion=" + strAccion);
		System.out.println("codigo=" + strCodigo);
		System.out.println("nombre=" + strNombre);
		System.out.println("apellido=" + strApellido);
		System.out.println("Direccion=" + strDireccion);

		System.out.println("DNI=" + strDni);
		System.out.println("DIPFA=" + strDIPFA);
		System.out.println("CUIL=" + strCUIL);
		
		
		strCUIL		=(String)request.getParameter("txtCUIL");

		
		
		System.out.println("aceptar=" + strAceptar);
		System.out.println("cancelar=" + strCancelar);
	
		//1- de acuerdo a la accion realizar lo siguiente
		// 	para el caso de modificar strAccion=M
		//   1.1- leer el objeto con el codigo y asignarlo a una variable de tipo carrera

		int iCodPersona = Integer.valueOf(strCodigo);
		
		//las accciones pueden se M, V, E
		// M y V tinen que ir aun jsp que muestre los dato del objeto
		// E, debe preguntar si esta seguro
		
		// 1- conectarse a la base de datos y la traer la carrera
		
		//2- acceder al modelo 
		//ConnectionManager cm = new ConnectionManager();
		ConnectionManager cm = new ConnectionManager();
	
		Persona  per;
			try {
				cm.conectar();
				cm.beginTx();
				PersonaDao perDao = new PersonaDao();
				// aca tengo la carrera buscada
				per = (Persona)perDao.leer(new Persona(iCodPersona), cm.getSession()).iterator().next();
		
				if (strAccion.equals("M") && strAceptar!=null){
					per.setNombre(strNombre);
					per.setApellido(strApellido);
					per.setDireccion(strDireccion);
					per.setTelefonoParticular(strTelefono);
					//se agregan los campos nuevos
					per.setDni(strDni);
					per.setIosfa(strDIPFA);
					per.setCuil(strCUIL);
					perDao.agregarModificar(per, cm.getSession());
				}else if(strAccion.equals("E") && strAceptar!=null){
					
					perDao.eliminar(per, cm.getSession());
				}
		//3 guardo la informacion en el request
				request.setAttribute("persona", per);
				request.setAttribute("accion",strAccion);
				getServletContext().getRequestDispatcher("/PersonaController").forward(request, response);
				cm.commitTx();
				cm.desconectar();
				} catch (SQLException e) {
			//TODO alguien -TODOS
			//1-en en request.setAtribute("error",e)
			//2-armar getServletContext().getRequestDispatcher("/presentacion/Error.jsp").forward(request, response);
			//3-crear Error.jsp donde se va a mostrar el error
			//4-recupear el atributo y el agregado de contacte con el administrador
				e.printStackTrace();
		} catch (ClassNotFoundException e) {
			String strMensaje= "No se pudo conectar a la base de datos";
			String strSolucion= "llamar Agust�n, el lo va a atender(creo)";
			request.setAttribute("mensajeError", strMensaje);
			request.setAttribute("solucion", strSolucion);
			request.setAttribute("error", e.getStackTrace());
			getServletContext().getRequestDispatcher("/presentacion/Error.jsp").forward(request, response);
			e.printStackTrace();
			
		} catch (Exception e) {
			String strMensaje= "Error desconocido";
			String strSolucion= "llamar Agust�n, el lo va a atender(creo)";
			request.setAttribute("mensajeError", strMensaje);
			request.setAttribute("solucion", strSolucion);
			
			e.printStackTrace();
		}
		
		
		
		//   1.2 -asignar al atributo descripcon el valor de strDescripcion
		//   1.3 - conectarse a la base de datos
		//   1.4 - llamar el metodo modificar enviando como parametro la carrera 
		//   1.5 - hacer un request dispatcher a carrera controller
		
	}
}

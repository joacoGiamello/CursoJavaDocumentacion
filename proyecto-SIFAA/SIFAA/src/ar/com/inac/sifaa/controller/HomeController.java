package ar.com.inac.sifaa.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ar.com.inac.sifaa.modelo.dao.PartidoDao;
import ar.com.inac.sifaa.util.ConnectionManager;

/**
 * Servlet implementation class HomeController
 */
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//1- debe realizar alguna regla de negocio
		
		Map<String, String> mapa = new HashMap<String, String>();
		mapa.put("1", "Personas")	;
		mapa.put("2", "Ingresos")	;
		
		//2-guardo informacion en el contexto
		
		getServletContext().setAttribute("mapa",mapa);
		ConnectionManager cm = new ConnectionManager();
		try {
			cm.conectar();
			cm.beginTx();			
			
			PartidoDao partDao = new PartidoDao();
			request.setAttribute("partidos", partDao.leer(null, cm.getSession()));
			//3- le paso el parametro a ComponenteView
			RequestDispatcher rd = request.getRequestDispatcher("/presentacion/ComponenteView.jsp");
			rd.forward(request, response);

			cm.commitTx();
			cm.desconectar();

		} catch (Exception e) {			
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			}

}

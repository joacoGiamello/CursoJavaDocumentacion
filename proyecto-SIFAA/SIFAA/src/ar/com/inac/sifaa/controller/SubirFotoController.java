package ar.com.inac.sifaa.controller;

import http.utils.multipartrequest.ServletMultipartRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import ar.com.inac.sifaa.modelo.Persona;
import ar.com.inac.sifaa.modelo.dao.PersonaDao;
import ar.com.inac.sifaa.util.ConnectionManager;
import ar.com.inac.sifaa.util.FileUtil;

/**
 * Servlet implementation class SubirFotoController
 */
public class SubirFotoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubirFotoController() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//antes que nada obtengo los datos
		
		//MultipartRequest  multi = new MultipartRequest(request,"c:/temporal,5000000");
		//MultipartRequest  multi = new MultipartRequest();
		ResourceBundle resources;
	    resources = ResourceBundle.getBundle("resources/fotos");
	    String strPath= resources.getString("path");
	    System.out.println(strPath);
	    
//		ServletMultipartRequest smr= new ServletMultipartRequest(request,strPath,5000000);
		 ServletMultipartRequest upload = new ServletMultipartRequest(
			       request,  strPath +"temp", 104857600); // max 100 mb
		 
		InputStream is 		= upload.getFileContents("Filedata");	
		 //TODO aca hay que manejarse como archivo
		String strAccion 		= upload.getURLParameter("txtAccion");
		String strCodPersona 	= upload.getURLParameter("txtCodigo");
		
		String strNomArch		= upload.getFileSystemName("Filedata");
		
		
		File dir = new File(strPath +"temp");
		String[] ficheros = dir.list();
		strNomArch = ficheros[0];
		File archivoTemp =new File(strPath +"temp/"+ strNomArch);
		FileInputStream fis =  new FileInputStream(archivoTemp);
		
		
		int iCodPersona 		= Integer.valueOf(strCodPersona);
		
		/*
		 * -----------------------------------------------
		 * obtengo los datos de la persona
		 * ----------------------------------------------
		 */
		//2- acceder al modelo 
		ConnectionManager cm = new ConnectionManager();
		
		Persona per;
		String strNomArchivo=null;
	try {
		cm.conectar();
		cm.beginTx();
		PersonaDao perDao = new PersonaDao();
		// aca obtengo los datos de la persona
		per = (Persona)perDao.leer(new Persona(iCodPersona), cm.getSession()).iterator().next();
		
		/*
		 * -----------------------------------------------
		 * 	 fin - obtengo los datos de la persona
		 * ----------------------------------------------
		 */
		strNomArchivo = per.getCodigo() + "_"+ per.getDni(); 
		
		//3 guardo la informacion en el request
		request.setAttribute("persona", per);
		request.setAttribute("accion",strAccion);

		String nomOriginal=null;
		String extension=null;

        nomOriginal= strNomArch;
        extension =FileUtil.getExtension(nomOriginal);
        if(extension.toUpperCase().equals("JPG") ||
           extension.toUpperCase().equals("BMP") ||
           extension.toUpperCase().equals("PNG") ) {
            		
            	per.setPathFoto(strNomArchivo+ "." + extension);
            	perDao.agregarModificar(per, cm.getSession());
            	
				
				/*y lo escribimos en el servido*/
				try {					
					grabarArchivo(fis,strPath + per.getPathFoto());
			        if(archivoTemp.delete())
			        	System.out.println("archivo eliminado");

				} catch (Exception e) {
					e.printStackTrace();
				}
            	}
            	else{
            		//ava va al jsp Error podemos informar la solucion
            	}
            	getServletContext().getRequestDispatcher("/PersonaController").forward(request, response);
            
        
		cm.commitTx();
		cm.desconectar();
		} catch (SQLException e) {			
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		 catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void grabarArchivo(FileInputStream pIs_foto, String pPahtYnomArch) throws IOException {
	    FileOutputStream fos = new FileOutputStream(pPahtYnomArch);
	        byte[] buffer = new byte[1024];
	        int len = 0;

	        while (len != (-1)) {
	          len = pIs_foto.read(buffer, 0, 1024);
	          if (len != (-1))
	            fos.write(buffer, 0, len);
	        }

	        fos.close();
	        pIs_foto.close();	
		
	}
}

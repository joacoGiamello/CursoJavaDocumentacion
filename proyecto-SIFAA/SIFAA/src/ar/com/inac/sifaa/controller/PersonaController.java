package ar.com.inac.sifaa.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ar.com.inac.sifaa.modelo.Partido;
import ar.com.inac.sifaa.modelo.Persona;
import ar.com.inac.sifaa.modelo.dao.PersonaDao;
import ar.com.inac.sifaa.util.ConnectionManager;



/**
 * Servlet implementation class PersonaController
 */
public class PersonaController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PersonaController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String strAccion = request.getParameter("accion");
		String strCodPersona = request.getParameter("codPersona");
		
		int iCodPersona = Integer.valueOf(strCodPersona);
		
		//las accciones pueden se M, V, E y F
		// M y V tinen que ir aun jsp que muestre los dato del objeto
		// E, debe preguntar si esta seguro
		
		// 1- conectarse a la base de datos y la traer la carrera
		
		//2- acceder al modelo 
		ConnectionManager cm = new ConnectionManager();
		
		Persona per;
try {
		cm.conectar();
		cm.beginTx();
		PersonaDao perDao = new PersonaDao();
		// aca obtengo los datos de la persona
		per = (Persona)perDao.leer(new Persona(iCodPersona), cm.getSession()).iterator().next();
	
		//3 guardo la informacion en el request
		request.setAttribute("persona", per);
		request.setAttribute("accion",strAccion);
		
		getServletContext().getRequestDispatcher("/presentacion/AccionPersona.jsp").forward(request, response);
		
		cm.commitTx();
		cm.desconectar();
		} catch (SQLException e) {
			
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		 catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//1- recibe el request txtDescripcion
		String strDiffa		= request.getParameter("txtDIPFA")		;
		String strNombre 	= request.getParameter("txtNombre")		;
		String strApellido 	= request.getParameter("txtApellido")	;
		String strDni		= request.getParameter("txtDNI")		;
		
		String strCuil		= request.getParameter("txtCUIL");		
		String strDireccion	= request.getParameter("txtDireccion")	;
		String strPartido	= request.getParameter("cmbPartido")	;
		//agregado para que no de  null pointer Exception
		Partido partido=null;
		if(strPartido!=null){
			String arrayPartido[]= strPartido.split("-");
			partido = new Partido(	Integer.parseInt(arrayPartido[0]), 
											arrayPartido[1])			;			
		}
		
		String strTelefono 	= request.getParameter("txtTelefono")	;
		String strTelefonoMovil = request.getParameter("txtTelefonoCel");
		String strEmail 	= request.getParameter("txtEmail") 		;
		String strMarca		= request.getParameter("txtMarca")		;
		
		String strModelo 	= request.getParameter("txtModelo")		;
		String strColor		= request.getParameter("txtColor")		;
		
		String strBuscar 	= request.getParameter("txtBuscar");
		String strAgregar 	= request.getParameter("txtAgregar");
		//TODO 1- Ariel, obtener todos los parametros y meterlos en variables
		
		Enumeration<String> enumParNombres = request.getParameterNames();
		while(enumParNombres.hasMoreElements()){
			String strNomParametro = enumParNombres.nextElement();
			System.out.print("\nparametro=" + strNomParametro);
			System.out.print("  -  valor=" + request.getParameter(strNomParametro));
		}
		
		//2- acceder al modelo 
		ConnectionManager cm = new ConnectionManager();
		List<Persona> personas;
try {
		cm.conectar();
		cm.beginTx();
		
		//Persona per = new Persona(strNombre, strApellido);
		Persona per = new Persona(	-1 		, strNombre	, strApellido	, strDireccion	,
									partido	, null		, null			, strDni		, 
									strCuil	, null		, null			, null			, 
									strDiffa, null		, null)							;
		//per.setDireccion(strDireccion);
		PersonaDao perDao = new PersonaDao();
		//para el agregar
		if(strAgregar!=null && strAgregar.equals("Agregar")){
			perDao.agregarModificar(per, cm.getSession());
			//de esta manera busca todos
			per.vaciar();
		}
		
		//todo esto es para el buscar
		personas = (List<Persona>) perDao.leer(per, cm.getSession());
		

		//3 guardo la informacion en el request
		request.setAttribute("personasList", personas);
		
		} catch (SQLException e) {
			
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		getServletContext().getRequestDispatcher("/presentacion/PersonaList.jsp").forward(request, response);
		cm.commitTx();
		try {
			cm.desconectar();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}

}

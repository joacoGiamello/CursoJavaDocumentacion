package colecciones;

import java.util.HashSet;
import java.util.TreeSet;

import sun.reflect.generics.tree.Tree;

public class VectorDinamicoSet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HashSet<String> hst= new HashSet<String>();
		hst.add("Gabriel");
		hst.add("Einstein");
		hst.add("Newton");
		hst.add("Galileo");
		
		hst.add("gabriel");
		hst.add("Newton");
		
		System.out.println("hst="+hst);
		
		TreeSet<String> hstOrd=new TreeSet<String>(hst);
		
		System.out.println("hstOrd="+hstOrd);

	}

}

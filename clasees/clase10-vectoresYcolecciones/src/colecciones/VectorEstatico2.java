package colecciones;

public class VectorEstatico2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//llenar un vector de tipo char con las 
		//letras del abcdario.
		
		//1- crear un vector con 26 posiciones
		char a[] = new char[26];
		
		//2- armar un cliclo que convierta el numero
		// en una letra
		//lleno el vector
		for (int i=0;i<a.length;i++){
			a[i]=(char)('A'+i);			
		}
		// lo muestro
		for (int i=0;i<a.length;i++){
			System.out.println(a[i]);			
		}
 
		
		

	}

}

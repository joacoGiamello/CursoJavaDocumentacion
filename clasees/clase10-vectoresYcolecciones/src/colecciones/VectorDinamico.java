package colecciones;

import java.util.ArrayList;
import java.util.Iterator;


public class VectorDinamico {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	 ArrayList<String> lst = new ArrayList<String>();
	 //llenar el string
	 lst.add("Gabriel");
	 lst.add("Gabriel");
	 lst.add("Gabriel");
	 lst.add("Pedro");
	 lst.add("Susanita");
	 lst.add("Daniela");
	 lst.remove("Susanita");
	 System.out.println("lista="+ lst);
	 
	 System.out.println("esta Gabriel =" + 
			 lst.contains("Gabriel"));
	 //como lo recorreria cualquera NO JAVA MUELTE..!!
	 //al que recorre una lista con get()
	 for(int i=0;i<lst.size();i++){
		 System.out.println(lst.get(i));
	 }
	 //iterator es para recorrer la lista
	 
	 Iterator<String> iter=lst.iterator();
	 while(iter.hasNext()){
		 System.out.println(iter.next());
	 }
	 
	 
	 
		

	}

}

package util;

public class StringUtil {
	public static boolean tieneLetra(String str){
		boolean isLetter=false;
		char ch=' ';
		//recorro el string str
		for (int i=0;i<str.length();i++){
			//toma el valor del caracter en la posicion i
			//12345g28
			//01234567
			
			ch=str.charAt(i);
			if(Character.isLetter(ch)){
				isLetter=true;
				break;
			}				
		}
		
		return isLetter;
	}
	public static boolean isEspacioDoble(String pCadena){		
		return pCadena.contains("  ");
	}
	
	public static boolean isNumero(String pCadena){
		int i=0;
		boolean tieneNumero=false;
		while(i<pCadena.length() && !tieneNumero )
			tieneNumero=Character.isDigit(pCadena.charAt(i++));	
		
		return tieneNumero;
	}

}

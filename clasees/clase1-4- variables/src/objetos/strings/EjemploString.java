package objetos.strings;

public class EjemploString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//1- defino la variable de referencia S
		//2- crear el objeto new
		//3- asignarle un valor "Hola"
		String s = new String("Hola");
		//llamo al metodo toUpperCase
		System.out.println("s en may="+ s.toUpperCase());
		System.out.println("s="+ s.toString());
		
		//le asigno otra referencia
		String s2=s;
		System.out.println("s2="+s2);

		//modifico el valor de s reemplazando
		//el String es inmutable
		System.out.println("reemplaza ="+s.replace('a', 'A'));
		System.out.println("s2 nuevo?="+s2);
		
		
		
	}

}

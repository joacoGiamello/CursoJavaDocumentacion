package objetos.calendar;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EjemploCalendar1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//creo un objto de tipo Calendar
		Calendar calAhora = Calendar.getInstance();
		//solamente para darle formato
		SimpleDateFormat sdf = 
			new SimpleDateFormat("yyyy/MM/dd EEEEEE hh:mm:ss");
		
		//Utilizo el formato con format y le paso como
		//parametro un Date
		System.out.println("ahora="+
			sdf.format(calAhora.getTime()));
		System.out.println("el a�o es="+ 
				calAhora.get(Calendar.YEAR));
		
		System.out.println("el mes es=" +
				calAhora.get(Calendar.MONTH));
		System.out.println("el dia es=" +
				calAhora.get(Calendar.DAY_OF_MONTH));
		System.out.println("el dia de hoy es "+
				calAhora.get(Calendar.DAY_OF_WEEK));
		
	}

}

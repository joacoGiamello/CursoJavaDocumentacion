package objetos.calendar;

import java.util.Date;

import util.FechaUtil;

public class PruebaCalendar {

	/**
	 * Esta clase es para probar la FechaUtil
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		//creo un objeto con la fecha de hoy
		FechaUtil.asDate("17/11/1972");
		int num = FechaUtil.asInt("17/11/1972");
		System.out.println("num="+ num);
		
		Date hoy = new Date();
		System.out.println("la fecha de hoy es=" + hoy );
		
		System.out.println("hoy "+ hoy+
					"fin de semana=" + FechaUtil.isFinDeSemana(hoy));
	}

}

package modulo1Teoria;

public class SwitchCase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int puesto =1;
		//resolverlo con if opcion 1
		System.out.println("if opcion 1\n");
		if(puesto==1)
			System.out.println("medalla de oro");		
		if(puesto==2)
			System.out.println("medalla de Plata");
		if(puesto==3)
			System.out.println("medalla de Bronce");		
		if(puesto>3)
			System.out.println("Gracias por participar");
	//resolviendo if segunda opcion
		System.out.println("if anidado opcion 2\n");
		if(puesto==1)
			System.out.println("medalla de oro");		
		else if(puesto==2)
			System.out.println("medalla de Plata");
		else if(puesto==3)
			System.out.println("medalla de Bronce");		
		else
			System.out.println("Gracias por participar");
		
		System.out.println("switch case opcion 3\n");
		
		switch (puesto) {
		case 1:
			System.out.println("medalla de Oro");
			break;			
		case 2:
			System.out.println("medalla de Plata");
			break;		
		case 3:
			System.out.println("medalla de Bronce");
			break;		
		default:
			System.out.println("Siga participando");
			break;
	}
	}
}

package modulo1Teoria;

public class Ciclos {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//quiero imprimir un nombre 10 veces
		
		//1- ciclo while
		System.out.println("ciclo while\n");
		int i=0;
		while (i<10){
			System.out.println("Gabriel "+i);
			//no es de buena costumbre usar break
			// para romper un ciclo
			if(i==5)
				break;
			i++;
		}
		
		System.out.println("\n\nciclo do while\n");
		i=0;
		do{
			System.out.println("Gabriel "+i);
			i++;			
		}while (i<10);
		
		System.out.println("\n\nciclo for\n");
		for(i=0;i<10;i++){
			System.out.println("Gabriel "+i);
		}

	}
}

	
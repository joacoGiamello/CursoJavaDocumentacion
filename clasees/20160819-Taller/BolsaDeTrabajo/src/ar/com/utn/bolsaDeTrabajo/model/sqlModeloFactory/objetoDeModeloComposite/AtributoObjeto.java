package ar.com.utn.bolsaDeTrabajo.model.sqlModeloFactory.objetoDeModeloComposite;

public class AtributoObjeto {
	private String nombreEnObjeto;
	private String nombreCampoEnTabla;
	private String tipoDatoComplemento;
	private String valor;
	public AtributoObjeto() {}
	

	public AtributoObjeto(String nombreEnObjeto, String nombreCampoEnTabla,
			String tipoDatoComplemento, String valor) {
		super();
		this.nombreEnObjeto = nombreEnObjeto;
		this.nombreCampoEnTabla = nombreCampoEnTabla;
		this.tipoDatoComplemento = tipoDatoComplemento;
		this.valor = valor;
	}


	public String getNombreEnObjeto() {
		return nombreEnObjeto;
	}

	public void setNombreEnObjeto(String nombreEnObjeto) {
		this.nombreEnObjeto = nombreEnObjeto;
	}

	public String getNombreCampoEnTabla() {
		return nombreCampoEnTabla;
	}

	public void setNombreCampoEnTabla(String nombreEnTabla) {
		this.nombreCampoEnTabla = nombreEnTabla;
	}

	public String getTipoDatoComplemento() {
		return tipoDatoComplemento;
	}

	public void setTipoDatoComplemento(String tipoDato) {
		this.tipoDatoComplemento = tipoDato;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
	
}

package ar.com.utn.bolsaDeTrabajo.model;

/**
 * @author Daniela
 * Esta clase indica un software y qu� c�digo le corresponde, para conocer el conocimiento del mismo 
 * que posee el alumno aplicante a un posible trabajo.
 */

public class Software {

/**
 * @param codigo,  indica el c�digo que corresponde a cada software espec�ficamente
 * @return int, el n�mero de c�digo que corresponde a un software espec�fico.
 * @param descripci�n, indica a qu� Software se refiere
 * @return String, el nombre del software de que se trate en palabras. EJ: Adobe Photoshop, 3D Studio Max, etc.
 */
	
	
	//constructores
	public Software (){}
	public Software (int pcodigo, String pdescripcion){
		codigo=pcodigo;
		descripcion = pdescripcion;
	}

	//atributos
	private int 		codigo;		
	private String		descripcion;
	
	//getters y setters
	public int getCodigo(){		return codigo;	}
	public void setCodigo(int pcodigo){ codigo = pcodigo;}
			
	public String getDescripcion(){		return descripcion;	}
	public void setDescripcion(String pdescripcion){ descripcion = pdescripcion;}

	//Equals
	public boolean equals(Object obj){
		boolean bln=false;
		Software software = null;
		
		if(obj instanceof Software){
			software=(Software)obj;
			//codigo= software.getCodigo();
			bln= codigo == software.getCodigo();
		}
		return bln;
	}
	
	//Hashcode
	@Override
	public int hashCode() {
		return codigo+descripcion.hashCode();
	}
	
	//ToString
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("C�digo = ");
		sb.append(codigo);
		sb.append("Descripci�n = ");
		sb.append(descripcion);

		
		return sb.toString();
	}
}



package ar.com.utn.bolsaDeTrabajo.model;

/**
 * @author Daniela
 * Esta clase indica los conocimientos de idioma que maneja el aplicante a un posible trabajo.
 */

public class ConocimientoIdioma {

/**
 * Esta clase eval�a qu� idiomas maneja el interesado, teniendo cada uno un c�digo identificatorio espec�fico
 * y distintos niveles de capacidad respecto del mismo.
 * @param codigo,  indica el c�digo que corresponda a cada idioma
 * @return int, un n�mero que es espec�fico a ese idioma. EJ: 08
 * @param idioma, indica el idioma que conoce
 * @return Idioma, el idioma en palabras. EJ: Alem�n ???????????????????????????
 * @param nivelDeConocimiento, indica el nivel de conocimiento que tiene respecto de dicho idioma
 * @return int, el nivel de conocimiento evaluado como escala num�rica (del 1 al 10???)????????????????????????
 */
	
	//constructores
	public ConocimientoIdioma (){}
	public ConocimientoIdioma (int pcodigo, Idioma pidioma, int pnivelDeConocimiento){
		codigo=pcodigo;
		idioma=pidioma;
		nivelDeConocimiento = pnivelDeConocimiento;
	}

	//atributos
	private int 	codigo;		
	private Idioma 	idioma; 
	private int		nivelDeConocimiento;
	
	//getters y setters
	public int getCodigo(){		return codigo;	}
	public void setCodigo(int pcodigo){ codigo = pcodigo;}
	
	public Idioma getIdioma(){		return idioma;	}
	public void setIdioma(Idioma pidioma){ idioma = pidioma;}
	
	public int getNivelDeConocimiento(){		return nivelDeConocimiento;	}
	public void setNivelDeConocimiento(int pniveldeconocimiento){ nivelDeConocimiento = pniveldeconocimiento;}
	
	//Equals
	public boolean equals(Object obj){
		boolean bln=false;
		ConocimientoIdioma conocimientoidioma = null;
		
		if(obj instanceof ConocimientoIdioma){
			conocimientoidioma=(ConocimientoIdioma)obj;
			bln= codigo == (conocimientoidioma.getCodigo());
		}
		return bln;
	}
	
	//Hashcode
	@Override
	public int hashCode() {
		return codigo+idioma.hashCode();
	}
	
	//ToString
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("C�digo = ");
		sb.append(codigo);
		sb.append("Idioma = ");
		sb.append(idioma);
		sb.append("Nivel de Conocimiento = ");
		sb.append(nivelDeConocimiento);
		
		return sb.toString();
	}

}
	 

	
	


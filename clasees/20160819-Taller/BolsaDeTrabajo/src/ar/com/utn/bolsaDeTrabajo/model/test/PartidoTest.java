package ar.com.utn.bolsaDeTrabajo.model.test;

import static org.junit.Assert.assertEquals;


import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.Partido;

public class PartidoTest {

	public static Partido partido;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		partido = new Partido();
		partido.setCodigo(3);
		partido.setDescripcion("Moron");

	}

	@Test
	public void setCodigoTest() {
		Assert.assertEquals(3, partido.getCodigo());
	}

	@Test
	public void setDescripcionTest() {
		assertEquals("Moron", partido.getDescripcion());
	}
	@Test
	public void equals_correctoTest() {
		Partido partido2 = new Partido(3, "Moron");
		Assert.assertTrue(partido.equals(partido2));
	}
	@Test
	public void equals_falsoTest() {
		Partido partido2 = new Partido(2, "San justo");
		Assert.assertFalse(partido.equals(partido2));
	}

}

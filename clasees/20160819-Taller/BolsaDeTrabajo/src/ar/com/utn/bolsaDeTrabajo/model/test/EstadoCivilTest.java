package ar.com.utn.bolsaDeTrabajo.model.test;

import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.EstadoCivil;


public class EstadoCivilTest {

	public static EstadoCivil estadocivil;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		estadocivil = new EstadoCivil();
		estadocivil.setCodigo(0);
		estadocivil.setDescripcion("Soltero/a");
	}
	@Test
	public void setCodigoTest() {
		Assert.assertEquals(0, estadocivil.getCodigo());
	}
		
	@Test
	public void setDescripcionTest() {
		assertEquals("Soltero/a", estadocivil.getDescripcion());
	}

	@Test
	public void equalsTest_correcto() {
		EstadoCivil estadocivil1 = new EstadoCivil(0, "Soltero/a");
		Assert.assertTrue(estadocivil.equals(estadocivil1));
	}
	@Test
	public void equalsTest_falso() {
		EstadoCivil estadocivil2 = new EstadoCivil(1, "Casado");
		Assert.assertFalse(estadocivil.equals(estadocivil2));
	}

}

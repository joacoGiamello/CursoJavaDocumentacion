package ar.com.utn.bolsaDeTrabajo.model;


public class Ciudad {

	public Ciudad(){}
	public Ciudad(int codigo, String descripcion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	
	private int codigo;
	private String descripcion;
	
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean equals(Object obj){
		boolean bln=false;
		Ciudad ciudad= null;		
		if(obj instanceof Ciudad){
			ciudad=(Ciudad) obj;
			bln =  ciudad.getCodigo()==codigo;
		}
		return bln;
	}
	@Override
	public int hashCode() {		
		return codigo;
	}
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=");
		sb.append(codigo);
		sb.append(",descripcion=");
		sb.append(descripcion);		
		return sb.toString();
	}
	
}	
DROP TABLE IF EXISTS `oferlab`.`experienciaslaborales`;
CREATE TABLE  `oferlab`.`experienciaslaborales` (
  `EXP_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EXP_ANTIGUEDAD` int(10) unsigned NOT NULL,
  `EXP_PUESTO` varchar(45) NOT NULL,
  `TIPC_CODIGO` int(10) unsigned NOT NULL,
  `CUR_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`EXP_CODIGO`),
  KEY `FK_experienciaslaborales_tipoDecontrato` (`TIPC_CODIGO`),
  KEY `FK_experienciaslaborales_curriculumVitae` (`CUR_CODIGO`),
  CONSTRAINT `FK_experienciaslaborales_curriculumVitae` FOREIGN KEY (`CUR_CODIGO`) REFERENCES `curriculumsvitae` (`CUR_CODIGO`),
  CONSTRAINT `FK_experienciaslaborales_tipoDecontrato` FOREIGN KEY (`TIPC_CODIGO`) REFERENCES `tiposdecontratos` (`TIPC_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `oferlab`.`tiposdecontratos`;
CREATE TABLE  `oferlab`.`tiposdecontratos` (
  `TIPC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPC_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`TIPC_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
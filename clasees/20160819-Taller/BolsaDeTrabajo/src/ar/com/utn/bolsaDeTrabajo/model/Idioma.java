package ar.com.utn.bolsaDeTrabajo.model;

/**
 * @author Daniela
 * Esta clase indica un idioma y qu� c�digo le corresponde, para conocer el conocimiento del mismo 
 * que posee el alumno aplicante a un posible trabajo.
 */

	public class Idioma {

/**
 * 
 * @param codigo,  indica el c�digo que corresponde a cada idioma espec�ficamente
 * @return int, el n�mero de c�digo que corresponde a un idioma espec�fico.
 * @param descripci�n, indica a qu� idioma se refiere
 * @return String, el idioma de que se trate en palabras. EJ: Alem�n, Ingl�s, etc.
 */

		
	//constructores
	public Idioma (){}
	public Idioma (int pcodigo, String pdescripcion){
	codigo=pcodigo;
	descripcion = pdescripcion;
	}
	//atributos
	private int 		codigo;		
	private String		descripcion;
	
	//getters y setters
	public int getCodigo(){		return codigo;	}
	public void setCodigo(int pcodigo){ codigo = pcodigo;}
			
	public String getDescripcion(){		return descripcion;	}
	public void setDescripcion(String pdescripcion){ descripcion = pdescripcion;}

	//Equals
	public boolean equals(Object obj){
	boolean bln=false;
	Idioma idioma = null;
		
		if(obj instanceof Idioma){
		idioma=(Idioma)obj;				
		bln= codigo==idioma.getCodigo();
		}
		
	return bln;
	}
		
	//Hashcode
	@Override
	public int hashCode() {
		return codigo;
	}
		
	//ToString
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("C�digo = ");
		sb.append(codigo);
		sb.append("Descripci�n = ");
		sb.append(descripcion);
			
		return sb.toString();
	}
}

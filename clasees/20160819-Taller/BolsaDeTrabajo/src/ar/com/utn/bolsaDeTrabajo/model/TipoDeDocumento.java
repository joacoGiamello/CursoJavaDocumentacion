package ar.com.utn.bolsaDeTrabajo.model;

/**
 * @author Daniela
 * Esta clase indica el tipo de documento y qu� c�digo le corresponde, a trav�s del cual el alumno interesado
 * acredita su identidad. Est� relacionado con documentaci�n de identidad oficial y actualizada.
 */

public class TipoDeDocumento {

/**
 * @param codigo,  indica el c�digo que corresponde a cada tipo posible de documento.
 * @return int, el n�mero de c�digo que corresponde a ese tipo espec�fico.
 * @param descripci�n, indica a qu� tipo de documento se refiere en palabras. 
 * @return String, el nombre del tipo de documento de que se trate. EJ: DNI, Pasaporte etc.
 */

	//constructores
	public TipoDeDocumento (){}
	public TipoDeDocumento (int pcodigo, String pdescripcion){
		codigo=pcodigo;
		descripcion = pdescripcion;
	}

	//atributos
	private int 	codigo;		
	private String		descripcion;
	
	//getters y setters
	public int getCodigo(){		return codigo;	}
	public void setCodigo(int pcodigo){ codigo = pcodigo;}
			
	public String getDescripcion(){		return descripcion;	}
	public void setDescripcion(String pdescripcion){ descripcion = pdescripcion;}

	//Equals
	public boolean equals(Object obj){
		boolean bln=false;
		TipoDeDocumento tipodedocumento = null;
		
		if(obj instanceof TipoDeDocumento){
			tipodedocumento=(TipoDeDocumento)obj;
			//codigo= tipodedocumento.getCodigo();
			bln= codigo == tipodedocumento.getCodigo();
		}
		return bln;
	}
	
	//Hashcode
	@Override
	public int hashCode() {
		return codigo+descripcion.hashCode();
	}
	
	//ToString
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("C�digo = ");
		sb.append(codigo);
		sb.append("Descripci�n = ");
		sb.append(descripcion);

		
		return sb.toString();
	}
}


package ar.com.utn.bolsaDeTrabajo.model;

/**
 * @author Daniela
 * Esta clase indica los conocimientos de software que maneja el aplicante a un posible trabajo.
 */

public class ConocimientoSoftware {
	
/**
 * Esta clase eval�a qu� software/s maneja el interesado, teniendo cada uno un c�digo identificatorio espec�fico
 * y distintos niveles de capacidad respecto del mismo.
 * @param codigo,  indica el c�digo que corresponda a cada software
 * @return int, un n�mero que es espec�fico a ese software. EJ: 01
 * @param Software, indica el software que conoce
 * @return Software, el nombre del Software. EJ: Adobe Illustrator ?????????????????????????
 * @param nivelDeConocimiento, indica el nivel de conocimiento que tiene respecto de dicho software
 * @return int, el nivel de conocimiento evaluado como escala num�rica (del 1 al 10???)???????????????????
 */
	
	
	//constructores
	public ConocimientoSoftware (){}
	public ConocimientoSoftware (int pcodigo, Software psoftware, int pnivelDeConocimiento){
		codigo=pcodigo;
		software=psoftware;
		nivelDeConocimiento = pnivelDeConocimiento;
	}

	//atributos
	private int 		codigo;		
	private Software 	software; 
	private int			nivelDeConocimiento;
	
	//getters y setters
	public int getCodigo(){		return codigo;	}
	public void setCodigo(int pcodigo){ codigo = pcodigo;}
	
	public Software getSoftware(){		return software;	}
	public void setSoftware(Software psoftware){ software = psoftware;}
	
	public int getNivelDeConocimiento(){		return nivelDeConocimiento;	}
	public void setNivelDeConocimiento(int pniveldeconocimiento){ nivelDeConocimiento = pniveldeconocimiento;}
	
	//Equals
	public boolean equals(Object obj){
		boolean bln=false;
		ConocimientoSoftware conocimientosoftware = null;
		
		if(obj instanceof ConocimientoSoftware){
			conocimientosoftware=(ConocimientoSoftware)obj;
			//codigo = conocimientosoftware.getCodigo();
			bln= codigo == conocimientosoftware.getCodigo();
		}
		return bln;
	}
	
	//Hashcode
	@Override
	public int hashCode() {
		return codigo+software.hashCode();
	}
	
	//ToString
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("C�digo = ");
		sb.append(codigo);
		sb.append("Software = ");
		sb.append(software);
		sb.append("Nivel de Conocimiento = ");
		sb.append(nivelDeConocimiento);
		
		return sb.toString();
	}

}
	 



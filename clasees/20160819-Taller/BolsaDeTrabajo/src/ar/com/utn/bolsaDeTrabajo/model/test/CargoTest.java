package ar.com.utn.bolsaDeTrabajo.model.test;
import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.Cargo;
import ar.com.utn.bolsaDeTrabajo.model.Partido;

public class CargoTest {
	public static Cargo cargo;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		cargo = new Cargo();
		cargo.setCodigo(3);
		cargo.setDescripcion("Moron");

	}

	@Test
	public void setCodigoTest() {
		Assert.assertEquals(3, cargo.getCodigo());
	}

	@Test
	public void setDescripcionTest() {
		assertEquals("Moron", cargo.getDescripcion());
	}
	@Test
	public void equals_correctoTest() {
		Cargo cargo2 = new Cargo(3, "Moron");
		Assert.assertTrue(cargo.equals(cargo2));
	}
	@Test
	public void equals_falsoTest() {
		Cargo cargo2 = new Cargo(2, "San justo");
		Assert.assertFalse(cargo.equals(cargo2));
	}
	
}

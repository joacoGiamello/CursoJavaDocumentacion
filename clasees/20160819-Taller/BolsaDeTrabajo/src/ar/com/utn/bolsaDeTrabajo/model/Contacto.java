package ar.com.utn.bolsaDeTrabajo.model;

public class Contacto {
	public Contacto(){}
	public Contacto(Cargo cargo) {
		//este es el que vale
		super();
		this.cargo = cargo;      }
	
	
	private Cargo cargo;


	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	

	@Override
	public boolean equals(Object obj) {
		boolean bln = false;
		Contacto con = null;
		if (obj instanceof Contacto){
			//down cast
			con = (Contacto)obj;
			bln = super.equals(obj);}
	return bln;
	}
	
	@Override
	public String toString() {
	StringBuffer sb =new StringBuffer(super.toString());
	//append suma
	sb.append(cargo);
	sb.append("cargo=");
	return sb.toString();
	
	
	}


	@Override
	public int hashCode() {
		return cargo.hashCode();
		}
	
	
	
	
	
	
                      }

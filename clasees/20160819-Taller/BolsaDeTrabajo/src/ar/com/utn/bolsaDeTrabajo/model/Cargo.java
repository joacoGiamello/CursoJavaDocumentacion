package ar.com.utn.bolsaDeTrabajo.model;

public class Cargo {

	//constructor
	public Cargo(){}
	public Cargo(int codigo ,String descripcion){
		
		
		
		super();
		this.codigo = codigo;    
		this.descripcion = descripcion;                     
												}
	//declaro codigo
	private int codigo;
	private String descripcion;
	
	//get and set
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		boolean bln = false;
		Cargo car = null;
		if (obj instanceof Cargo){
			//down cast
			car = (Cargo)obj;
			bln = super.equals(obj) && 
			car.getCodigo()==codigo;;}
	return bln;
	}
	
	
	@Override
	public String toString() {
	StringBuffer sb =new StringBuffer(super.toString());
	//append suma
	sb.append(codigo);
	sb.append("codigo=");
	sb.append(descripcion);
	sb.append("descricion=");
	return sb.toString();
	
	
                        	}

	@Override
	public int hashCode() {
		return codigo+
			   descripcion.hashCode(); 	
		
		
		
	}
	
}

DROP TABLE IF EXISTS `oferlab`.`curriculumsvitae`;
CREATE TABLE  `oferlab`.`curriculumsvitae` (
  `CUR_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CUR_ANIOFINSECUNDARIO` int(10) unsigned NOT NULL,
  `CUR_CONFIDENCIALIDAD` char(1) NOT NULL,
  `CUR_INSTITUTO` varchar(45) NOT NULL,
  `TIPS_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CUR_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
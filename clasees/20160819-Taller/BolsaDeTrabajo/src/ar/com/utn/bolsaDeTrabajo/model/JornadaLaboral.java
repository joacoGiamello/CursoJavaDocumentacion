/**
 * @author Sebastian
 *	corresponde a los detalles de la jornada laboral
 */
package ar.com.utn.bolsaDeTrabajo.model;

import java.util.Date;
import java.util.List;


public class JornadaLaboral {
	public JornadaLaboral (){}
	public JornadaLaboral(int codigo, int diaHora, List diaDeLaSemana,
			Date fechaDesde) {
		//este es el que vale
		super();
		this.codigo = codigo;
		this.diaHora = diaHora;
		this.diaDeLaSemana = diaDeLaSemana;
		this.fechaDesde = fechaDesde;
	}
	
	private int codigo;
	private int diaHora;
	private List diaDeLaSemana;
	private Date fechaDesde;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getDiaHora() {
		return diaHora;
	}
	public void setDiaHora(int diaHora) {
		this.diaHora = diaHora;
	}
	public List getDiaDeLaSemana() {
		return diaDeLaSemana;
	}
	public void setDiaDeLaSemana(List diaDeLaSemana) {
		this.diaDeLaSemana = diaDeLaSemana;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
public boolean equals(Object obj) {
		
		boolean bln=false;
		JornadaLaboral per =null;
		if(obj instanceof JornadaLaboral){
		
			per=(JornadaLaboral) obj;
			bln= per.getCodigo()== codigo;
		}
		return bln;
	}	
@Override
public int hashCode(){
		return codigo;
}
@Override
	public String toString() {
		StringBuffer sb =new StringBuffer("codigo=");
		sb.append(codigo);
		
	return sb.toString();
		
	}
}

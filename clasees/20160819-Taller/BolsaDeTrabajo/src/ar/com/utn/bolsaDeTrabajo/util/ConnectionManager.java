package ar.com.utn.bolsaDeTrabajo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
		
public class ConnectionManager {
	private Connection con ;
	public void conectar() throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		
//		con = DriverManager.getConnection(	"jdbc:mysql://192.168.15.20:3306/oferlab",
//											"sistema",
//											"sistema");
		con = DriverManager.getConnection(	"jdbc:mysql://localhost:3306/oferlab",
				"sistema",
				"sistema");
	}

public Connection getConnection(){
	return con;
}
public void cerrar() throws SQLException{
	con.close();
}
}
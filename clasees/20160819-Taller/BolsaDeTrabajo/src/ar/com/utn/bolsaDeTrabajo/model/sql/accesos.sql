DROP TABLE IF EXISTS `oferlab`.`accesos`;
CREATE TABLE  `oferlab`.`accesos` (
  `ACC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ACC_DESCRIPCION` varchar(45) NOT NULL,
  `ROL_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ACC_CODIGO`),
  KEY `FK_accesos_rol` (`ROL_CODIGO`),
  CONSTRAINT `FK_accesos_rol` FOREIGN KEY (`ROL_CODIGO`) REFERENCES `roles` (`ROL_CODIGO`)
)
DROP TABLE IF EXISTS `oferlab`.`jornadaslaborales`;
CREATE TABLE  `oferlab`.`jornadaslaborales` (
  `JOR_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `JOR_DIAHORA` int(10) unsigned NOT NULL,
  `JOR_FECHADESDE` date NOT NULL,
  PRIMARY KEY (`JOR_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
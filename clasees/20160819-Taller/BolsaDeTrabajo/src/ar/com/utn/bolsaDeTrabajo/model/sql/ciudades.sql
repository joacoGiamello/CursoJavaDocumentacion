DROP TABLE IF EXISTS `oferlab`.`ciudades`;
CREATE TABLE  `oferlab`.`ciudades` (
  `CIU_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CIU_DESCRIPCION` varchar(45) NOT NULL,
  `PAR_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CIU_ID`),
  KEY `FK_ciudades_partidos` (`PAR_ID`),
  CONSTRAINT `FK_ciudades_partidos` FOREIGN KEY (`PAR_ID`) REFERENCES `partidos` (`PAR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
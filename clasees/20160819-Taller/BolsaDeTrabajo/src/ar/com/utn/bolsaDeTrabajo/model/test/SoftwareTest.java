package ar.com.utn.bolsaDeTrabajo.model.test;

import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.Software;

public class SoftwareTest {
	
	public static Software software;
	
	@BeforeClass
	public static void setUpBeforeClass(){
		software = new Software();
		software.setCodigo(3);
		software.setDescripcion("Adobe Illustrator");
	}
	@Test
	public void setCodigoTest() {
		Assert.assertEquals(3, software.getCodigo());
	}
		
	@Test
	public void setDescripcionTest() {
		assertEquals("Adobe Illustrator", software.getDescripcion());
	}

	@Test
	public void equalsTest_correcto() {
		Software software1 = new Software(3, "Adobe Illustrator");
		Assert.assertTrue(software.equals(software1));
	}
	@Test
	public void equalsTest_falso() {
		Software software2 = new Software(2, "Adobe Photoshop");
		Assert.assertFalse(software.equals(software2));
	}

}

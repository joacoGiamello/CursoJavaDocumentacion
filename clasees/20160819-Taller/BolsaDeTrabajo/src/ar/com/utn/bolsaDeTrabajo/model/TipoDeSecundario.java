package ar.com.utn.bolsaDeTrabajo.model;
/**
 * @author Sebastian
 *	corresponde al tipo de secundario de los alumnos
 */

public class TipoDeSecundario {
	public TipoDeSecundario (){}
	
	public TipoDeSecundario(int codigo, String descripcion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	private int codigo;
	private String descripcion;
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
public boolean equals(Object obj) {
		
		boolean bln=false;
		TipoDeSecundario per =null;
		if(obj instanceof TipoDeSecundario){
		
			per=(TipoDeSecundario) obj;
			bln= per.getCodigo()== codigo;
		}
		return bln;
	}		
	
@Override
public int hashCode(){
		return codigo;
}
@Override
	public String toString() {
		StringBuffer sb =new StringBuffer("codigo=");
		sb.append(codigo);
		
	return sb.toString();
		
	}	
	
	
}
package ar.com.utn.bolsaDeTrabajo.model.exceptions;

public class ModeloException extends Exception {

	public ModeloException(String string) {
		super(string);
	}
}

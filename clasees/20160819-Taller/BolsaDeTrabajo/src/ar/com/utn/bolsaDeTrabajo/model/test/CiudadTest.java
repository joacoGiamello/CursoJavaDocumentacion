package ar.com.utn.bolsaDeTrabajo.model.test;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.Ciudad;

public class CiudadTest {

	public static Ciudad ciudad;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ciudad = new Ciudad();
		ciudad.setCodigo(1);
		ciudad.setDescripcion("Moron");

	}

	@Test
	public void setCodigoTest() {
		assertEquals(1, ciudad.getCodigo());
	}

	@Test
	public void setDescripcionTest() {
		assertEquals("Moron", ciudad.getDescripcion());
	}

	@Test
	public void equals_correctoTest() {
		Ciudad ciudad2 = new Ciudad(1, "Moron");
		Assert.assertTrue(ciudad.equals(ciudad2));
	}

	@Test
	public void equals_falseTest() {
		Ciudad ciudad2 = new Ciudad(2, "La Matanza");
		Assert.assertFalse(ciudad.equals(ciudad2));
	}
}

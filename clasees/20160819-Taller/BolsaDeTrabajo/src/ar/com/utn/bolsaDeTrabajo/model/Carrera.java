package ar.com.utn.bolsaDeTrabajo.model;

/**
 * @author Sebastian
 *	corresponde a las carreras de los alumnos
 */

public class Carrera {
	public Carrera (){}
	
	public Carrera(int codigo, String descripcion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private int codigo;
	private String descripcion;
	
	public boolean equals(Object obj) {
		
		boolean bln=false;
		Carrera per =null;
		if(obj instanceof Carrera){
		
			per=(Carrera) obj;
			bln= per.getCodigo()== codigo;
		}
		return bln;
	}
	@Override
	public int hashCode(){
			return codigo;
	}
	@Override
		public String toString() {
			StringBuffer sb =new StringBuffer("codigo=");
			sb.append(codigo);
			
		return sb.toString();
			
		}
}

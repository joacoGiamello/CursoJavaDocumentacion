DROP TABLE IF EXISTS `oferlab`.`diashoras`;
CREATE TABLE  `oferlab`.`diashoras` (
  `DH_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DH_HORAINICIO` int(10) unsigned NOT NULL,
  `DH_HORAFIN` int(10) unsigned NOT NULL,
  `DH_MININICIO` int(10) unsigned NOT NULL,
  `DH_MINFIN` int(10) unsigned NOT NULL,
  `JOR_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`DH_CODIGO`),
  KEY `FK_diashoras_joranada` (`JOR_CODIGO`),
  CONSTRAINT `FK_diashoras_joranada` FOREIGN KEY (`JOR_CODIGO`) REFERENCES `jornadaslaborales` (`JOR_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
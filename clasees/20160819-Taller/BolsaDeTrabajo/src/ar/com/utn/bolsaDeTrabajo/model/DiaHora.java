package ar.com.utn.bolsaDeTrabajo.model;
/**
 * @author Sebastian
 *	corresponde al detalle del dia.
 */

public class DiaHora {
 public DiaHora(){}
 
 public DiaHora(int dia, int horaFin, int horaInicio, int minutoFin,
			int minitoInicio) {
		super();
		this.dia = dia;
		this.horaFin = horaFin;
		this.horaInicio = horaInicio;
		this.minutoFin = minutoFin;
		this.minutoInicio = minitoInicio;
	}
 
 private int dia;
 private int horaFin;
 private int horaInicio;
 private int minutoFin;
 private int minutoInicio;
public int getDia() {
	return dia;
}

public void setDia(int dia) {
	this.dia = dia;
}

public int getHoraFin() {
	return horaFin;
}

public void setHoraFin(int horaFin) {
	this.horaFin = horaFin;
}

public int getHoraInicio() {
	return horaInicio;
}

public void setHoraInicio(int horaInicio) {
	this.horaInicio = horaInicio;
}

public int getMinutoFin() {
	return minutoFin;
}

public void setMinutoFin(int minutoFin) {
	this.minutoFin = minutoFin;
}

public int getMinutoInicio() {
	return minutoInicio;
}

public void setMinutoInicio(int minutoInicio) {
	this.minutoInicio = minutoInicio;
}
public boolean equals(Object obj) {
	
	boolean bln=false;
	DiaHora pr =null;
	if(obj instanceof DiaHora){
	
		pr=(DiaHora) obj;
		bln= 	pr.getDia()			== dia 			&& 
				pr.getHoraFin()		== horaFin		&&
				pr.getHoraInicio()	== horaInicio	&&
				pr.getMinutoFin()	== minutoFin	&&
				pr.getMinutoInicio()== minutoInicio;
	}
	return bln;
}
public int hashCode(){
		return dia + horaFin + horaInicio + minutoFin + minutoInicio;

}
@Override
public String toString() {
StringBuffer sb =new StringBuffer(super.toString());
sb.append("dia=");
sb.append(dia);

sb.append("horaFin=");
sb.append(horaFin);

sb.append("horaInicio=");
sb.append(horaInicio);

sb.append("minutoFin=");
sb.append(minutoFin);

sb.append("minutoInicio=");
sb.append(minutoInicio);

return sb.toString();
}
}


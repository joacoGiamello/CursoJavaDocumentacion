package ar.com.utn.bolsaDeTrabajo.model.test;

import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.TipoDeDocumento;

public class TipoDeDocumentoTest {
	
	public static TipoDeDocumento tipodedocumento;
	
	@BeforeClass
	public static void setUpBeforeClass(){
		tipodedocumento = new TipoDeDocumento();
		tipodedocumento.setCodigo(1);
		tipodedocumento.setDescripcion("DNI");
	}
	@Test
	public void setCodigoTest() {
		Assert.assertEquals(1, tipodedocumento.getCodigo());
	}
		
	@Test
	public void setDescripcionTest() {
		assertEquals("DNI", tipodedocumento.getDescripcion());
	}

	@Test
	public void equalsTest_correcto() {
		TipoDeDocumento tipodedocumento1 = new TipoDeDocumento(1, "DNI");
		Assert.assertTrue(tipodedocumento.equals(tipodedocumento1));
	}
	@Test
	public void equalsTest_falso() {
		TipoDeDocumento tipodedocumento2 = new TipoDeDocumento(2, "Pasaporte");
		Assert.assertFalse(tipodedocumento.equals(tipodedocumento2));
	}
}

package ar.com.utn.bolsaDeTrabajo.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.Pais;

public class PaisTest {

	public static Pais pais;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		pais = new Pais();
		pais.setCodigo(2);
		pais.setDescripcion("Argentina");
	}

	@Test
	public void setCodigoTest() {
		assertEquals(2, pais.getCodigo());
	}

	@Test
	public void setDescripcionTest() {
		assertEquals("Argentina", pais.getDescripcion());
	}

	@Test
	public void equals_correctoTest() {
		Pais pais2 = new Pais(2, "Argentina");
		Assert.assertTrue(pais.equals(pais2));
	}

	@Test
	public void equals_falsoTest() {
		Pais pais2 = new Pais(1, "Brasil");
		Assert.assertFalse(pais.equals(pais2));
	}
}

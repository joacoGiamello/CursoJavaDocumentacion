package ar.com.utn.bolsaDeTrabajo.model.test;

import ar.com.utn.bolsaDeTrabajo.model.Partido;

public class CarreraTest {

	public static Carrera Carrera;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Carrera = new Carrera();
		Carrera.setCodigo(3);
		Carrera.setDescripcion("Ing");

	}

	@Test
	public void setCodigoTest() {
		Assert.assertEquals(3, partido.getCodigo());
	}

	@Test
	public void setDescripcionTest() {
		assertEquals("Moron", partido.getDescripcion());
	}
	@Test
	public void equalsTest_correcto() {
		Partido partido2 = new Partido(3, "Moron");
		Assert.assertTrue(partido.equals(partido2));
	}
	@Test
	public void equalsTest_falso() {
		Partido partido2 = new Partido(2, "San justo");
		Assert.assertFalse(partido.equals(partido2));
	}

}

}

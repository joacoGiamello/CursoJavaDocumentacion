package objetos;

import java.util.Date;

import util.StringUtil;


/**
 * 
 * @author gcasas
 * corresponde a una persona
 */
public class Persona {
	//constructor
	public Persona(){}
	public Persona(String nom){
		setNombre(nom);
	}
	
	//atributos
	private String 	nombre			;
	private String 	apellido		;
	private Date 	fechaNacimiento	;
	
	//metodos
	/**
	 * obtengo el valor del atributo nombre
	 */
	public String getNombre(){
		return nombre;
	}
	/**
	 * asinga el valor al aributo nombre
	 * validando que no tenga dos espacios
	 * @param nom
	 * 
	 */
	public void setNombre(String nom){
		
		//if(nom.contains("  "))
		if(StringUtil.containsDoubleSpace(nom))
			nombre=null;
		else
			nombre=nom;
	}
	
	public String getApellido() {		return apellido;		}
	public void setApellido(String ape) {		apellido = ape;	}
	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	
	public void setFechaNacimiento(Date fec) {
		fechaNacimiento = fec;
	}
	
	public boolean equals(Object obj){
		//aca escribo las reglas para decir
		boolean bln=false;
		Persona per = null;		
		if(obj instanceof Persona){
			//down-cast
			per=(Persona) obj;
			bln = nombre.equals(per.getNombre()) &&
				  apellido.equals(per.getApellido());
		}
		return bln;
	}
	@Override
	public int hashCode() {		
		return nombre.hashCode() + 
			   apellido.hashCode();
	}
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("nombre=");
		sb.append(nombre);
		sb.append(",apellido=");
		sb.append(apellido);
		sb.append(",fechaDeNacimiento=");
		sb.append(fechaNacimiento);			
		return sb.toString();
	}

}

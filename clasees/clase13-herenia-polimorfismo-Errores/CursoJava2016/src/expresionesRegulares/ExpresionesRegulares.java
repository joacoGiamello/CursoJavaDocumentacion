package expresionesRegulares;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpresionesRegulares {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Pattern pattern=null;
		//3 numeros y un espacio
		pattern= Pattern.compile("[1-9][1-9][1-9]\\s");
		//3 numeros 
		pattern= Pattern.compile("[1-9][1-9][1-9]");
		//3 letras
		pattern= Pattern.compile("[A-Z][A-Z][A-Z]");
		//3 @ y un numero
		pattern= Pattern.compile("@[1-9]");
		//3 @ y numeros hasta que termite
		pattern= Pattern.compile("@[1-9]*");
		//1 numero y letras hasta que termite
		pattern= Pattern.compile("([1-9][A-Z]*)(@)([0-9]*)");
		// como corroboro un email.
		
		Matcher matcher= pattern.matcher("AP2UNTAR@12345678 ADENTRO");
		if(matcher.find()){
			//0 es el total
			System.out.println("matcher.group(0)=" +matcher.group(0));
			//1 es el primer grupo (([1-9][A-Z]*))
			System.out.println("matcher.group(1)=" +matcher.group(1));
			//2 es el segunto grupo (@)
			System.out.println("matcher.group(2)=" +matcher.group(2));
			//3 es el tercer grupo ([0-9]*)
			
			System.out.println("matcher.group(3)=" +matcher.group(3));
			
		}else
			System.out.println("no se encontro nada");
// quiero corroborar un email
		
//		pattern= Pattern.compile("([A-Z]*[a-z]*[1-9]*)(@)([a-z]*[A-Z]*[1-9]*)(.com)");
		pattern= Pattern.compile("(\\w+)(@)(\\w+)(.com)");		
		// como corroboro un email.
		
		matcher= pattern.matcher("gca12sas1972@gmail.com");
		System.out.println("\n\nemail\n");
		if(matcher.find()){
			System.out.println("matcher.group(0)=" +matcher.group(0));
			System.out.println("matcher.group(1)=" +matcher.group(1));
			System.out.println("matcher.group(2)=" +matcher.group(2));
			System.out.println("matcher.group(3)=" +matcher.group(3));
			System.out.println("matcher.group(4)=" +matcher.group(4));
		}else
			System.out.println("email incorrecto");
		
		

	}

}

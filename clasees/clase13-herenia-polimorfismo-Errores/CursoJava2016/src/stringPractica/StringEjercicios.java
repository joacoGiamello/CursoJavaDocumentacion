package stringPractica;

public class StringEjercicios {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//recorrer el string buscar la cantidad de 
		//letras a
		String str = new String("Hola Argentina amiga mia");
		//para recorrer el string
		//length me da la longitud
		int cantLetraA=0;
		for(int i=0;i<str.length();i++){
			char letra = str.charAt(i);
			//si es una a hay que contarla
			if(letra=='a' || letra=='A')
				cantLetraA++;
			
		}
		System.out.println("la cantidad de a es="+
									cantLetraA);

	}

}

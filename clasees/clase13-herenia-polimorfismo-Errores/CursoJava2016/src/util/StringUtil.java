package util;

/**
 * @author gcasas
 * Esta clase tiene la finalidad de dar todos los servicios
 * relativos a cadenas de caracteres.
 */
public class StringUtil {
	
	/**
	 * Este m�todo sirve para determinar si una cadena es capicua
	 * @param s, corresponde a la cadena a evaluar
	 * @return true si es capicua y false no lo es
	 */
	public static boolean isCapicua(String s){
		boolean blnResult=true;
		// lo que quieran}
		int i=0;
		while(i<s.length()/2 && blnResult){
			if(s.charAt(i)==s.charAt(s.length()-i-1))
				blnResult=true;
			else
				blnResult=false;
			i++;
		}
		
		return blnResult;
	}
	public static boolean containsDoubleSpace(String str){
		boolean isDobleSpace=false;
	// recorrer el 
		int i=0;
		while (i<str.length()-1 && !isDobleSpace) {
			char letra=str.charAt(i);
			char letraSiguiente=str.charAt(i+1);
			if(letra==' ' && letraSiguiente== ' ')
				isDobleSpace=true;
			i++;
		}		
		return isDobleSpace;
	}
}

package modulo2;

import java.util.Scanner;

public class EjemploString3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//creo un string 
		System.out.println("ingrese nombre");
		Scanner scn = new Scanner(System.in);
	    String strNombre = scn.nextLine();
	    
		System.out.println("ingrese apellido");		
	    String strApellido = scn.nextLine();
	    
	    String strTotal = strNombre + " " + strApellido;
	    System.out.println("total=" + strTotal);
	    
// 		String s1 = new String("Este es el curso de java de los viernes");
//		//s1.length() me devuelve la longitud
//		for (int i=0;i<s1.length();i++){
//			System.out.println("pos " + i + "-"+ s1.charAt(i));
//		}

	}

}

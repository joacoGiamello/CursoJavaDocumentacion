package modulo2;

public class EjemploString2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	//cada vez que trabajo con objetos
	//1- definicion String s1
	//2- la crecion, new String..
	//3- asignacion, String("Hola") -- constructor	
		
		String s1 = new String("Hola");
		String s2 = new String ("Hola");
		String s3 =  null;
		if(s1==s2)
			System.out.println("estan en la misma posicion");
		else
			System.out.println("estan en posiciones diferentes");
		
		if(s1.equals(s2))
			System.out.println("tienen el mismo contenido");
		else
			System.out.println("tienen contenido diferente");
		
		System.out.println("Mayuscula " + s1.toUpperCase());
		System.out.println("Minuscula " + s1.toLowerCase());
		
		s3= s1.replace('a', '4').replace('o', '5');
		
		
		System.out.println("s1 vale "+ s1);
		System.out.println("s1 vale "+ s1.toString());
		System.out.println("s3 vale " + s3);
		
		
	}

}

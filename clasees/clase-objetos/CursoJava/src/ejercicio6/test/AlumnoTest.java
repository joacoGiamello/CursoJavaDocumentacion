package ejercicio6.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ejercicio6.Alumno;

public class AlumnoTest {
	Alumno alu;

	@Before
	public void setUp() throws Exception {
		alu = new Alumno("10" 				,
				   		 "Gabriel Casas"	, 
				   		 "1111111111"		,
				   		 "gcasas@gmail.com"	, 
				   		 "17/11/1972")		;
		
	}

	@Test
	public void testSetNombre() {
		alu.setNombre("Gabriel Casas");
		assertEquals("Gabriel Casas", alu.getNombre());
	}
	@Test
	public void testSetNombreEspacios() {
		alu.setNombre("Gabriel  Casas");
		assertEquals("", alu.getNombre());
	}
	@Test
	public void testSetNombreNumeros() {
		alu.setNombre("Gabriel123");
		assertEquals("", alu.getNombre());
	}

	@Test
	public void testSetEmailCorrecto() {
		alu.setEmail("gcasas@gmail.com");
		assertEquals("gcasas@gmail.com", alu.getEmail());
	}

	@Test
	public void testSetEmailFallidoPrincipio() {
		alu.setEmail("@gcasasgmail.com");
		assertEquals("", alu.getEmail());
	}

	@Test
	public void testSetEmailFallidoFinal() {
		alu.setEmail("gcasasgmail.com@");
		assertEquals("", alu.getEmail());
	}
	@Test
	public void testSetEmailEspacio() {
		alu.setEmail("gcasas gmail.com");
		assertEquals("", alu.getEmail());
	}
	
}

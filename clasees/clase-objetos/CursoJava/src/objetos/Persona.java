package objetos;

import java.util.Date;

import util.CalendarUtil;

public class Persona {
	private String 	nombre				;
	private String 	apellido			;
	private Date	fechaDeNacimiento	;
	
	public Persona(){
		nombre 		= new String("Gabrielito")	;
		apellido 	= new String("Casas")		;
		fechaDeNacimiento=CalendarUtil.asDate("17/11/1972");
	}
	public Persona(String pNombre,String pApellido, Date pNac){
		nombre				= pNombre;
		apellido			= pApellido;
		fechaDeNacimiento	= pNac;
	}
	public void setNombre(String pNom){
		nombre = pNom;
	}
	public String getNombre(){
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String pApe) {
		apellido = pApe;
	}
	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	public void setFechaDeNacimiento(Date pNac) {
		fechaDeNacimiento = pNac;
	}
	
	

}

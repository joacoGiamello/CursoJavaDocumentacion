package objetos.test;

import static org.junit.Assert.*;

import java.util.Calendar;

import objetos.Persona;

import org.junit.Before;
import org.junit.Test;

import util.CalendarUtil;

public class PersonaTest {
	Persona perVacio;
	Persona perParam;

	@Before
	public void setUp() throws Exception {
		perVacio = new Persona();
		perParam = new Persona("Albert", "Einstein", CalendarUtil.asDate("01/05/1942"));
	}

	@Test
	public void testPersona_nombre() {
		assertEquals("Gabrielito", perVacio.getNombre());		
	}
	@Test
	public void testPersona_apellido() {
		assertEquals("Casas", perVacio.getApellido());		
	}

	@Test
	public void testPersona_FechaNac() {
		Calendar cal = Calendar.getInstance();
		cal.set(1972, Calendar.NOVEMBER, 17);
		
		assertEquals(cal.getTime(), perVacio.getFechaDeNacimiento());		
	}
	
	@Test
	public void testPersonaStringStringDate() {
		
	}

}

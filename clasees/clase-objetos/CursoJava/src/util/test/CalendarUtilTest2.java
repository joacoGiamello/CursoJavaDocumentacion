package util.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import util.CalendarUtil;

public class CalendarUtilTest2 {
	Date fechaConocida, otraFechaConocida;
	

	@Before
	public void setUp() throws Exception {
		Calendar cal = Calendar.getInstance();
		//fecha de mi nacimiento
		cal.set(1972, Calendar.NOVEMBER, 17);		
		fechaConocida=cal.getTime();
		// es sabado
		cal.set(2016, Calendar.NOVEMBER, 12);
		otraFechaConocida = cal.getTime();
		
	}

	@Test
	public void testGetAnio() {
		assertEquals(1972, CalendarUtil.getAnio(fechaConocida));
	}

	@Test
	public void testIsFinDeSemanTrue() {
		assertTrue(CalendarUtil.isFinDeSeman(otraFechaConocida));
	}
	@Test
	public void testIsFinDeSemanFalse() {
		
		assertFalse(CalendarUtil.isFinDeSeman(new Date()));
	}


}

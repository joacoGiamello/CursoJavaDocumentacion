package modulo1;

public class SwitchCiclo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//gernerar un codigo ASCII al hazar y contar 
		//la cantidad de a, A, b, B, c,C y otros
		int num=0;
		int conta=0,contA=0,contb=0,contB=0,contc=0,contC=0;
		int contOtros=0;
		char ascii;
		for(int i=0;i<20;i++){
			do{
			num = (int)(Math.random()*1000%256);
			}while(!(num>64 && num<122));
			
			ascii =(char)num;
			System.out.println("ascii="+ascii);
			switch (ascii) {
			case 'a':
				conta++;
				break;
			case 'A':
				contA++;
				break;
			case 'b':
				contb++;
				break;
			case 'B':
				contB++;
				break;
			case 'c':
				contc++;
				break;
			case 'C':
				contC++;
				break;
			default:
				contOtros++;
				break;
			}
		}
		System.out.println("cantidad de 'a'="+conta);
		System.out.println("cantidad de 'A'="+contA);
		System.out.println("cantidad de 'b'="+contb);
		System.out.println("cantidad de 'B'="+contB);
		System.out.println("cantidad de 'c'="+contc);
		System.out.println("cantidad de 'C'="+contC);
		System.out.println("cantidad de otros="+contOtros);
		
	}

}

package modulo1;

public class EjemploCiclos {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int i =0;
		System.out.println("utilizando ciclo while");
		while (i<10){
			System.out.println("El valor de i es " +i);
			//i=i+1;
			i++;
		}
		
		System.out.println("\n\nciclo for");
		
		for(i=0;i<10;i++){
			System.out.println("El valor de i es " +i);
		}
		System.out.println("\n\nCiclo do while");
		i=0;
		do{
			System.out.println("el valor de i es " + i);
			i++;
		}while(i<10);
		
	}

}
